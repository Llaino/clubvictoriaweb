/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misModelos.Consultas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import misModelos.Beans.ModeloEmpleadoJavaBeans;

/**
 *
 * @author vivi
 */
public class ModeloEmpleadoPOJOs {
    // consultas a databaseClub
    // para consultas necesita Connection

    private String jdbcDriver;
    private String dbName;
    private String urlRoot;
    private ArrayList<ModeloEmpleadoJavaBeans> resultado;
    private ActionListener listener;

    public String usuario;

    public ModeloEmpleadoPOJOs(String url, String dbName) {

        jdbcDriver = "com.mysql.jdbc.Driver";
        urlRoot = "jdbc:mysql://" + url + "/";
        this.dbName = dbName;
        listener = null;

        resultado = new ArrayList<ModeloEmpleadoJavaBeans>();

        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            reportException(e.getMessage());

        }
    }

    /*
     * Consultas tareas de Administración de Altas Bajas y Modificaciones a los registros de Empleados
     * 
     *   
     */
    public boolean consultarAdministracionEmpleado(String abm, String id, String codigo, String nombre, String direccion, String telefono, String especialidad, String password, String estado) throws SQLException {
        if (abm.equals("nuevoRegistroEmpleado")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                // VERIFICO SI EL NOMBRE NO FUE CARGADO EN EL PASADO, PARA EVITAR REPETIR NOMBRES EN LA TABLA USUARIOS
                Statement stVerificacionNombre = con.createStatement();
                stVerificacionNombre.execute("Select id from `usuarios` where `nombre`='" + nombre + "'");
                ResultSet rsVerificacionNombre = stVerificacionNombre.getResultSet();
                if (!rsVerificacionNombre.next()) {

                    // INSERT EN LA TABLA EMPLEADOS
                    Statement stNuevoRegistro = con.createStatement();
                    stNuevoRegistro.executeUpdate("Insert into `empleados` (`codigo`, `nombre`, `direccion`, `telefono`, `especialidad`, `password`, `estado` ) values ('" + codigo + "', '" + nombre + "', '" + direccion + "','" + telefono + "', '" + especialidad + "', '" + password + "', '" + estado + "')");
                    stNuevoRegistro.close();

                    // NECESITO UNA VARIABLE PARA GUARDAR EL ULTIMO ID INGRESADO
                    String idEmpleado = "";
                    // OBTENGO EL ULTIMO ID INGRESADO
                    Statement stConsultaId = con.createStatement();
                    stConsultaId.execute("Select id from `empleados` where `nombre`='" + nombre + "'");

                    ResultSet rsConsultaId = stConsultaId.getResultSet();
                    while (rsConsultaId.next()) {
                        idEmpleado = rsConsultaId.getString(1);
                    }

                    stConsultaId.close();

                    // INSERT EN LA TABLA USUARIOS, INCLUYO LA VARIABLE IDEMPLEADO
                    Statement stNuevoUsuario = con.createStatement();
                    stNuevoUsuario.executeUpdate("Insert into `usuarios` (`idUsuario`,`nombre`, `tipo`, `password`, `estado` ) values ('" + idEmpleado + "', '" + nombre + "', 'Empleado', '" + password + "', '" + estado + "')");
                    stNuevoUsuario.close();

                } else {
                    stVerificacionNombre.close();
                    // CIERRA CONEXIÓN
                    con.close();

                    // EN EL CASO DE QUE EL NOMBRE YA EXISTIA, NO SE PUEDE REPETIR, SE DEVUELVE FALSE
                    return false;
                }
                stVerificacionNombre.close();
                // CIERRA CONEXIÓN
                con.close();

            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        } else if (abm.equals("modificarRegistroEmpleado")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");

                // VERIFICO SI EL NOMBRE NO FUE CARGADO EN EL PASADO, PARA EVITAR REPETIR NOMBRES EN LA TABLA USUARIOS
                Statement stVerificacionNombre = con.createStatement();
                stVerificacionNombre.execute("Select id from `usuarios` where `nombre`='" + nombre + "'");
                ResultSet rsVerificacionNombre = stVerificacionNombre.getResultSet();
                if (!rsVerificacionNombre.next()) {

                    // UPDATE EN LA TABLA USUARIOS
                    Statement stUsuarios = con.createStatement();
                    stUsuarios.executeUpdate("update `usuarios` set `nombre`='" + nombre + "', `password`='" + password + "', `estado`='" + estado + "' where `idUsuario`='" + id + "'");
                    stUsuarios.close();

                    // UPDATE EN LA TABLA EMPLEADOS
                    Statement stEmpleados = con.createStatement();
                    stEmpleados.executeUpdate("update `empleados` set `codigo`='" + codigo + "', `nombre`='" + nombre + "', `direccion`='" + direccion + "', `telefono`='" + telefono + "', `especialidad`='" + especialidad + "', `password`='" + password + "', `estado`='" + estado + "' where `id`='" + id + "'");
                    stEmpleados.close();

                } else {
                    stVerificacionNombre.close();
                    // CIERRA CONEXIÓN
                    con.close();

                    // EN EL CASO DE QUE EL NOMBRE YA EXISTIA, NO SE PUEDE REPETIR, SE DEVUELVE FALSE
                    return false;
                }
                stVerificacionNombre.close();
                // CIERRA CONEXIÓN
                con.close();

            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        } else if (abm.equals("modificarEmpleado")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                Statement stmt = con.createStatement();
                stmt.execute("Select * from empleados");

                ResultSet rs = stmt.getResultSet();
                while (rs.next()) {
                    ModeloEmpleadoJavaBeans mjb = new ModeloEmpleadoJavaBeans();
                    // es necesario que este en la misma orden que la tabla 
                    mjb.setId(rs.getString(1));// id
                    mjb.setCodigo(rs.getString(2));
                    mjb.setNombre(rs.getString(3));
                    mjb.setDireccion(rs.getString(4));
                    mjb.setTelefono(rs.getString(5));
                    mjb.setEspecialidad(rs.getString(6));
                    mjb.setPassword(rs.getString(7));
                    mjb.setEstado(rs.getString(8)); // estado del socio
                    resultado.add(mjb);
                }

                stmt.close();
                con.close();
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }

        } else if (abm.equals("eliminarEmpleado")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                Statement stmt = con.createStatement();
                stmt.execute("Select * from empleados");

                ResultSet rs = stmt.getResultSet();
                while (rs.next()) {
                    ModeloEmpleadoJavaBeans mjb = new ModeloEmpleadoJavaBeans();
                    // es necesario que este en la misma orden que la tabla 
                    mjb.setId(rs.getString(1));// id
                    mjb.setCodigo(rs.getString(2));
                    mjb.setNombre(rs.getString(3));
                    mjb.setDireccion(rs.getString(4));
                    mjb.setTelefono(rs.getString(5));
                    mjb.setEspecialidad(rs.getString(6));
                    mjb.setPassword(rs.getString(7));
                    mjb.setEstado(rs.getString(8)); // estado del socio
                    resultado.add(mjb);
                }

                stmt.close();
                con.close();
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }

        } else if (abm.equals("eliminarRegistroEmpleado")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                Statement statement = con.createStatement();
                statement.executeUpdate("delete from `empleados` where `id`='" + id + "'");

                statement.close();
                con.close();

            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        } else if (abm.equals("datosEmpleado")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                Statement stmt = con.createStatement();
                stmt.execute("Select * from empleados");

                ResultSet rs = stmt.getResultSet();
                while (rs.next()) {
                    ModeloEmpleadoJavaBeans mjb = new ModeloEmpleadoJavaBeans();
                    // es necesario que este en la misma orden que la tabla 
                    mjb.setId(rs.getString(1));// id
                    mjb.setCodigo(rs.getString(2));
                    mjb.setNombre(rs.getString(3));
                    mjb.setDireccion(rs.getString(4));
                    mjb.setTelefono(rs.getString(5));
                    mjb.setEspecialidad(rs.getString(6));
                    mjb.setPassword(rs.getString(7));
                    mjb.setEstado(rs.getString(8)); // estado del socio
                    resultado.add(mjb);
                }

                stmt.close();
                con.close();
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        }
        return true;
    }


    /*
     *  
     * 
     *   
     */
    public ArrayList<ModeloEmpleadoJavaBeans> getResultado() {
        return resultado;
    }

    private void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }

    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }

}
