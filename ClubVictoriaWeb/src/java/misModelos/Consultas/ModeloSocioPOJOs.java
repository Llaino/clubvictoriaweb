/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misModelos.Consultas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import misModelos.Beans.ModeloSocioJavaBeans;

/**
 *
 * @author vivi
 */
public class ModeloSocioPOJOs {
    // consultas a databaseClub
    // para consultas necesita Connection

    private String jdbcDriver;
    private String dbName;
    private String urlRoot;
    private ArrayList<ModeloSocioJavaBeans> resultado;
    private ActionListener listener;

    public String usuario;

    public ModeloSocioPOJOs(String url, String dbName) {

        jdbcDriver = "com.mysql.jdbc.Driver";
        urlRoot = "jdbc:mysql://" + url + "/";
        this.dbName = dbName;
        listener = null;

        resultado = new ArrayList<ModeloSocioJavaBeans>();

        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            reportException(e.getMessage());

        }
    }

    /*
     * Consultas tareas de Administración de Altas Bajas y Modificaciones a los registros de Socios
     * 
     *   
     */
    public boolean consultarAdministracionSocio(String abm, String id, String nombre, String dni, String direccion, String telefono, String ingreso, String password, String estado) throws SQLException {
        if (abm.equals("nuevoRegistroSocio")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");

                // VERIFICO SI EL NOMBRE NO FUE CARGADO EN EL PASADO, PARA EVITAR REPETIR NOMRES EN LA TABLA USUARIOS
                Statement stVerificacionNombre = con.createStatement();
                stVerificacionNombre.execute("Select id from `usuarios` where `nombre`='" + nombre + "'");
                ResultSet rsVerificacionNombre = stVerificacionNombre.getResultSet();
                if (!rsVerificacionNombre.next()) {

                    // INSERT EN LA TABLA SOCIOS
                    Statement stNuevoRegistro = con.createStatement();
                    stNuevoRegistro.executeUpdate("Insert into `socios` (`nombre`, `dni`, `direccion`, `ingreso`, `password`, `estado` ) values ('" + nombre + "', '" + dni + "', '" + direccion + "', '" + ingreso + "', '" + password + "', 'Habilitado')");
                    stNuevoRegistro.close();

                    // NECESITO UNA VARIABLE PARA GUARDAR EL ULTIMO ID INGRESO
                    String idSocio = "";
                    // OBTENGO EL ULTIMO ID INGRESADO
                    Statement stConsultaId = con.createStatement();
                    stConsultaId.execute("Select id from `socios` where `nombre`='" + nombre + "'");

                    ResultSet rsConsultaId = stConsultaId.getResultSet();
                    while (rsConsultaId.next()) {
                        idSocio = rsConsultaId.getString(1);
                    }

                    stConsultaId.close();

                    // INSERT EN LA TABLA USUARIOS, INCLUYO LA VARIABLE IDSOCIO
                    Statement stNuevoUsuario = con.createStatement();
                    stNuevoUsuario.executeUpdate("Insert into `usuarios` (`idUsuario`,`nombre`, `tipo`, `password`, `estado` ) values ('" + idSocio + "', '" + nombre + "', 'Socio', '" + password + "', '" + estado + "')");
                    stNuevoUsuario.close();

                } else {
                    stVerificacionNombre.close();
                    // CIERRA CONEXIÓN
                    con.close();

                    // EN EL CASO DE QUE EL NOMBRE YA EXISTIA, NO SE PUEDE REPETIR, SE DEVUELVE FALSE
                    return false;
                }
                stVerificacionNombre.close();
                // CIERRA CONEXIÓN
                con.close();

            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        } else if (abm.equals("modificarRegistroSocio")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");

                // VERIFICO SI EL NOMBRE NO FUE CARGADO EN EL PASADO, PARA EVITAR REPETIR NOMRES EN LA TABLA USUARIOS
                Statement stVerificacionNombre = con.createStatement();
                stVerificacionNombre.execute("Select id from `usuarios` where `nombre`='" + nombre + "'");
                ResultSet rsVerificacionNombre = stVerificacionNombre.getResultSet();

                if (!rsVerificacionNombre.next()) {

                    // UPDATE EN LA TABLA USUARIOS
                    Statement stUsuaios = con.createStatement();
                    stUsuaios.executeUpdate("update `usuarios` set `nombre`='" + nombre + "', `password`='" + password + "', `estado`='" + estado + "' where `idUsuario`='" + id + "'");
                    stUsuaios.close();

                    // UPDATE EN LA TABLA SOCIOS
                    Statement stSocios = con.createStatement();
                    stSocios.executeUpdate("update `socios` set `nombre`='" + nombre + "', `dni`='" + dni + "', `direccion`='" + direccion + "', `telefono`='" + telefono + "', `ingreso`='" + ingreso + "', `password`='" + password + "', `estado`='" + estado + "' where `id`='" + id + "'");
                    stSocios.close();
                } else {
                    stVerificacionNombre.close();
                    // CIERRA CONEXIÓN
                    con.close();

                    // EN EL CASO DE QUE EL NOMBRE YA EXISTIA, NO SE PUEDE REPETIR, SE DEVUELVE FALSE
                    return false;
                }
                stVerificacionNombre.close();
                // CIERRA CONEXIÓN
                con.close();

            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        } else if (abm.equals("modificarSocio")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                Statement stmt = con.createStatement();
                stmt.execute("Select * from socios");

                ResultSet rs = stmt.getResultSet();
                while (rs.next()) {
                    ModeloSocioJavaBeans mjb = new ModeloSocioJavaBeans();
                    // es necesario que este en la misma orden que la tabla 
                    mjb.setId(rs.getString(1));// id
                    mjb.setNombre(rs.getString(2));
                    mjb.setDni(rs.getString(3));
                    mjb.setDireccion(rs.getString(4));
                    mjb.setTelefono(rs.getString(5));
                    mjb.setIngreso(rs.getString(6));
                    mjb.setPassword(rs.getString(7));
                    mjb.setEstado(rs.getString(8)); // estado del socio
                    resultado.add(mjb);
                }

                stmt.close();
                con.close();
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }

        } else if (abm.equals("eliminarSocio")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                Statement stmt = con.createStatement();
                stmt.execute("Select * from socios");

                ResultSet rs = stmt.getResultSet();
                while (rs.next()) {
                    ModeloSocioJavaBeans mjb = new ModeloSocioJavaBeans();
                    // es necesario que este en la misma orden que la tabla 
                    mjb.setId(rs.getString(1));// id
                    mjb.setNombre(rs.getString(2));
                    mjb.setDni(rs.getString(3));
                    mjb.setDireccion(rs.getString(4));
                    mjb.setTelefono(rs.getString(5));
                    mjb.setIngreso(rs.getString(6));
                    mjb.setPassword(rs.getString(7));
                    mjb.setEstado(rs.getString(8)); // estado del socio
                    resultado.add(mjb);
                }

                stmt.close();
                con.close();
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }

        } else if (abm.equals("eliminarRegistroSocio")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                Statement statement = con.createStatement();
                //statement.executeUpdate("Insert into `usuarios` (`idUsuario`,  `nombre`, `tipo`, `password`, `estado`) values ('" + idUsuario + "', '" + nombre + "', '" + tipoUsuario + "', '" + password + "', 'Habilitado')");
                statement.executeUpdate("delete from `socios` where `id`='" + id + "'");

                statement.close();
                con.close();

            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        } else if (abm.equals("datosSocio")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                Statement stmt = con.createStatement();
                stmt.execute("Select * from socios");

                ResultSet rs = stmt.getResultSet();
                while (rs.next()) {
                    ModeloSocioJavaBeans mjb = new ModeloSocioJavaBeans();
                    // es necesario que este en la misma orden que la tabla 
                    mjb.setId(rs.getString(1));// id
                    mjb.setNombre(rs.getString(2));
                    mjb.setDni(rs.getString(3));
                    mjb.setDireccion(rs.getString(4));
                    mjb.setTelefono(rs.getString(5));
                    mjb.setIngreso(rs.getString(6));
                    mjb.setPassword(rs.getString(7));
                    mjb.setEstado(rs.getString(8)); // estado del socio
                    resultado.add(mjb);
                }

                stmt.close();
                con.close();
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        }
        return true;
    }


    /*
     *  
     * 
     *   
     */
    public ArrayList<ModeloSocioJavaBeans> getResultado() {
        return resultado;
    }

    private void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }

    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }

}
