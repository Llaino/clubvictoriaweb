/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misModelos.Consultas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import misModelos.Beans.ModeloZonaJavaBeans;

/**
 *
 * @author vivi
 */
public class ModeloZonaPOJOs {
    // consultas a databaseClub
    // para consultas necesita Connection

    private String jdbcDriver;
    private String dbName;
    private String urlRoot;
    private ArrayList<ModeloZonaJavaBeans> resultado;
    private ActionListener listener;

    public String usuario;

    public ModeloZonaPOJOs(String url, String dbName) {

        jdbcDriver = "com.mysql.jdbc.Driver";
        urlRoot = "jdbc:mysql://" + url + "/";
        this.dbName = dbName;
        listener = null;

        resultado = new ArrayList<ModeloZonaJavaBeans>();

        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            reportException(e.getMessage());

        }
    }

    /*
     * Consultas tareas de Administración de Altas Bajas y Modificaciones a los registros de Zonas
     * 
     *   
     */
    public boolean consultarAdministracionZona(String abm, String id, String letra, String tipo, String cantidad, String profundidad, String ancho, String estado) throws SQLException {
        if (abm.equals("nuevoRegistroZona")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                // VERIFICO SI LA LETRA NO FUE CARGADA EN EL PASADO, PARA EVITAR REPETIR LETRA DE ZONA
                Statement stVerificacionLetra = con.createStatement();
                stVerificacionLetra.execute("Select id from `zonas` where `letra`='" + letra + "'");
                ResultSet rsVerificacionNombre = stVerificacionLetra.getResultSet();
                if (!rsVerificacionNombre.next()) {

                    // INSERT EN LA TABLA ZONAS
                    Statement stNuevoRegistro = con.createStatement();
                    stNuevoRegistro.executeUpdate("Insert into `zonas` (`letra`, `tipo`, `cantidad`, `profundidad`, `ancho`, `estado` ) values ('" + letra + "', '" + tipo + "', '" + cantidad + "','" + profundidad + "', '" + ancho + "', '" + estado + "')");
                    stNuevoRegistro.close();
                } else {
                    stVerificacionLetra.close();
                    // CIERRA CONEXIÓN
                    con.close();

                    // EN EL CASO DE QUE LA LETRA YA EXISTIA, NO SE PUEDE REPETIR, SE DEVUELVE FALSE
                    return false;
                }
                stVerificacionLetra.close();
                // CIERRA CONEXIÓN
                con.close();

            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        } else if (abm.equals("modificarRegistroZona")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");

                // VERIFICO SI LA LETRA NO FUE CARGADA EN EL PASADO, PARA EVITAR REPETIR LETRA DE ZONA
                Statement stVerificacionLetra = con.createStatement();
                stVerificacionLetra.execute("Select id from `zonas` where `letra`='" + letra + "'");
                ResultSet rsVerificacionLetra = stVerificacionLetra.getResultSet();
                if (!rsVerificacionLetra.next()) {

                    // UPDATE EN LA TABLA ZONAS
                    Statement statement = con.createStatement();
                    statement.executeUpdate("update `zonas` set `letra`='" + letra + "', `tipo`='" + tipo + "', `cantidad`='" + cantidad + "', `profundidad`='" + profundidad + "', `ancho`='" + ancho + "', `estado`='" + estado + "' where `id`='" + id + "'");
                    statement.close();
                } else {
                    stVerificacionLetra.close();
                    // CIERRA CONEXIÓN
                    con.close();

                    // EN EL CASO DE QUE LA LETRA YA EXISTIA, NO SE PUEDE REPETIR, SE DEVUELVE FALSE
                    return false;
                }
                stVerificacionLetra.close();
                // CIERRA CONEXIÓN
                con.close();

            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        } else if (abm.equals("modificarZona")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                Statement stmt = con.createStatement();
                stmt.execute("Select * from zonas");

                ResultSet rs = stmt.getResultSet();
                while (rs.next()) {
                    ModeloZonaJavaBeans mjb = new ModeloZonaJavaBeans();
                    // es necesario que este en la misma orden que la tabla 
                    mjb.setId(rs.getString(1));// id
                    mjb.setLetra(rs.getString(2)); // letra asignada a la zona
                    mjb.setTipo(rs.getString(3)); // tipo de barco
                    mjb.setCantidad(rs.getString(4)); // cantidad de barcos
                    mjb.setProfundidad(rs.getString(5)); // profundidad del amarre
                    mjb.setAncho(rs.getString(6)); // ancho del amarre
                    mjb.setEstado(rs.getString(7)); // estado de la zona
                    resultado.add(mjb);
                }

                stmt.close();
                con.close();
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }

        } else if (abm.equals("eliminarZona")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                Statement stmt = con.createStatement();
                stmt.execute("Select * from zonas");

                ResultSet rs = stmt.getResultSet();
                while (rs.next()) {
                    ModeloZonaJavaBeans mjb = new ModeloZonaJavaBeans();
                    // es necesario que este en la misma orden que la tabla 
                    mjb.setId(rs.getString(1));// id
                    mjb.setLetra(rs.getString(2)); // letra asignada a la zona
                    mjb.setTipo(rs.getString(3)); // tipo de barco
                    mjb.setCantidad(rs.getString(4)); // cantidad de barcos
                    mjb.setProfundidad(rs.getString(5)); // profundidad del amarre
                    mjb.setAncho(rs.getString(6)); // ancho del amarre
                    mjb.setEstado(rs.getString(7)); // estado de la zona
                    resultado.add(mjb);
                }

                stmt.close();
                con.close();
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }

        } else if (abm.equals("eliminarRegistroZona")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                Statement statement = con.createStatement();
                //statement.executeUpdate("Insert into `usuarios` (`idUsuario`,  `nombre`, `tipo`, `password`, `estado`) values ('" + idUsuario + "', '" + nombre + "', '" + tipoUsuario + "', '" + password + "', 'Habilitado')");
                statement.executeUpdate("delete from `zonas` where `id`='" + id + "'");

                statement.close();
                con.close();

            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        } else if (abm.equals("datosZona")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                Statement stmt = con.createStatement();
                stmt.execute("Select * from zonas");

                ResultSet rs = stmt.getResultSet();
                while (rs.next()) {
                    ModeloZonaJavaBeans mjb = new ModeloZonaJavaBeans();
                    // es necesario que este en la misma orden que la tabla 
                    mjb.setId(rs.getString(1));// id
                    mjb.setLetra(rs.getString(2)); // nombre
                    mjb.setTipo(rs.getString(3)); // direccion
                    mjb.setCantidad(rs.getString(4)); // direccion
                    mjb.setProfundidad(rs.getString(5)); // ingreso
                    mjb.setAncho(rs.getString(6)); // password
                    mjb.setEstado(rs.getString(7)); // estado
                    resultado.add(mjb);
                }

                stmt.close();
                con.close();
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        }
        return true;
    }

    /*
     *  
     * 
     *   
     */
    public ArrayList<ModeloZonaJavaBeans> getResultado() {
        return resultado;
    }

    private void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }

    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }

}
