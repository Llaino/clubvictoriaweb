/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misModelos.Consultas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import misModelos.Beans.*;

public class ModeloABMPOJOs {
    // consultas a databaseClub
    // para consultas necesita Connection

    private String jdbcDriver;
    private String dbName;
    private String urlRoot;
    private ArrayList<ModeloABMJavaBeans> resultado;
    private ArrayList<ModeloSocioJavaBeans> resultadoS;
    private ArrayList<ModeloEmpleadoJavaBeans> resultadoE;
    private ArrayList<ModeloZonaJavaBeans> resultadoZ;
    private ActionListener listener;

    public String usuario;

    public ModeloABMPOJOs(String url, String dbName) {

        jdbcDriver = "com.mysql.jdbc.Driver";
        urlRoot = "jdbc:mysql://" + url + "/";
        this.dbName = dbName;
        listener = null;

        resultado = new ArrayList<ModeloABMJavaBeans>();
        resultadoS = new ArrayList<ModeloSocioJavaBeans>();
        resultadoE = new ArrayList<ModeloEmpleadoJavaBeans>();
        resultadoZ = new ArrayList<ModeloZonaJavaBeans>();

        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            reportException(e.getMessage());
        }
    }

    /*
     * Consultar Login 
     * 
     *   
     */
    public String consultarLogin(String nombre, String password) {
        String tipoUsuario = ""; // TIPO DE USUARIO
        String tipoEstado = ""; // ESTADO DE USUARIO
        try {
            // CONEXIÓN A LA BASE DE DATOS
            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();

            // CONSULTO A LA BASE DE DATOS, A VER SI EL NOMBRE Y LA CONTRASEÑA EXISTEN
            stmt.execute("SELECT * FROM `usuarios` where `nombre`='" + nombre + "' and `password`='" + password + "'");
            ResultSet rs = stmt.getResultSet();

            while (rs.next()) {
                ModeloABMJavaBeans jb = new ModeloABMJavaBeans();
                jb.setId(rs.getString(1)); // ID SECUENCIAL
                jb.setIdUsuario(rs.getString(2)); // RELACION TABLA USUARIOS CON LA TABLA ADMINISTRADORES
                jb.setNombre(rs.getString(3));
                jb.setTipo(rs.getString(4)); // TIPO DE USUARIO
                jb.setPassword(rs.getString(5));
                jb.setEstado(rs.getString(6)); // HABILITADO/ DESHABILITADO

                resultado.add(jb);

                tipoUsuario = rs.getString(4); // TIPO DE USUARIO
                tipoEstado = rs.getString(6); // ESTADO DE USUARIO
            }
            con.close();
            stmt.close();

        } catch (SQLException e) {
            reportException(e.getMessage());
        }

        if (tipoUsuario.equals("")) {
            return "Nombre o contraseña no corresponden"; // DEVUELVO QUE EL USUARIO NO EXISTE
        } else if (tipoEstado.equals("Deshabilitado")) {
            return "Usuario Deshabilitado"; // DEVUELVO EL ESTADO DE USUARIO
        }
        return tipoUsuario; // DEVUELVO EL TIPO DE USUARIO: ADMINISTRADOR, SOCIO O EMPLEADO
    }

    /*
     * Consultas tareas de Administración de Altas Bajas y Modificaciones a los registros de Administradores
     * 
     *   
     */
    public boolean consultarAdministracionAdministrador(String abm, String id, String nombre, String dni, String direccion, String ingreso, String password, String estado) throws SQLException {

        if (abm.equals("nuevoRegistro")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");

                // VERIFICO SI EL NOMBRE NO FUE CARGADO EN EL PASADO, PARA EVITAR REPETIR NOMBRES EN LA TABLA USUARIOS
                Statement stVerificacionNombre = con.createStatement();
                stVerificacionNombre.execute("Select id from `usuario` where `nombre`='" + nombre + "'");
                ResultSet rsVerificacionNombre = stVerificacionNombre.getResultSet();
                if (!rsVerificacionNombre.next()) {

                    // INSERT EN LA TABLA ADMINISTRADORES
                    Statement stNuevoRegistro = con.createStatement();
                    stNuevoRegistro.executeUpdate("Insert into `administradores` (`nombre`, `dni`, `direccion`, `ingreso`, `password`, `estado` ) values ('" + nombre + "', '" + dni + "', '" + direccion + "', '" + ingreso + "', '" + password + "', 'Habilitado')");
                    stNuevoRegistro.close();

                    // NECESITO UNA VARIABLE PARA GUARDAR EL ULTIMO ID INGRESADO
                    String idAdmin = "";
                    // OBTENGO EL ULTIMO ID INGRESADO
                    Statement stConsultaId = con.createStatement();
                    stConsultaId.execute("Select id from `administradores` where `nombre`='" + nombre + "'");

                    ResultSet rsConsultaId = stConsultaId.getResultSet();
                    while (rsConsultaId.next()) {
                        idAdmin = rsConsultaId.getString(1);
                    }

                    stConsultaId.close();

                    // INSERT EN LA TABLA USUARIOS, INCLUYO LA VARIABLE IDEMPLEADO
                    Statement stNuevoUsuario = con.createStatement();
                    stNuevoUsuario.executeUpdate("Insert into `usuarios` (`idUsuario`,`nombre`, `tipo`, `password`, `estado` ) values ('" + idAdmin + "', '" + nombre + "', 'Administrador', '" + password + "', 'Habilitado')");
                    stNuevoUsuario.close();

                } else {
                    stVerificacionNombre.close();
                    // CIERRA CONEXIÓN
                    con.close();

                    // EN EL CASO DE QUE EL NOMBRE YA EXISTIA, NO SE PUEDE REPETIR, SE DEVUELVE FALSE
                    return false;
                }
                stVerificacionNombre.close();
                // CIERRA CONEXIÓN
                con.close();

            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        } else if (abm.equals("modificarRegistro")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");

                // VERIFICO SI EL NOMBRE NO FUE CARGADO EN EL PASADO, PARA EVITAR REPETIR NOMBRES EN LA TABLA USUARIOS
                Statement stVerificacionNombre = con.createStatement();
                stVerificacionNombre.execute("Select id from `usuario` where `nombre`='" + nombre + "'");
                ResultSet rsVerificacionNombre = stVerificacionNombre.getResultSet();
                if (!rsVerificacionNombre.next()) {

                    // UPDATE EN LA TABLA USUARIOS
                    Statement stUsuarios = con.createStatement();
                    stUsuarios.executeUpdate("update `usuarios` set `nombre`='" + nombre + "', `password`='" + password + "', `estado`='" + estado + "' where `idUsuario`='" + id + "'");
                    stUsuarios.close();

                    // UPDATE EN LA TABLA ADMINISTRADORES
                    Statement stAdministradores = con.createStatement();
                    stAdministradores.executeUpdate("update `administradores` set `nombre`='" + nombre + "', `dni`='" + dni + "', `direccion`='" + direccion + "', `ingreso`='" + ingreso + "', `password`='" + password + "', `estado`='" + estado + "' where `id`='" + id + "'");
                    stAdministradores.close();

                } else {
                    stVerificacionNombre.close();
                    // CIERRA CONEXIÓN
                    con.close();

                    // EN EL CASO DE QUE EL NOMBRE YA EXISTIA, NO SE PUEDE REPETIR, SE DEVUELVE FALSE
                    return false;
                }
                stVerificacionNombre.close();
                // CIERRA CONEXIÓN
                con.close();

            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        } else if (abm.equals("modificarAdministrador")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                Statement stmt = con.createStatement();
                stmt.execute("Select * from administradores");

                ResultSet rs = stmt.getResultSet();
                while (rs.next()) {
                    ModeloABMJavaBeans mjb = new ModeloABMJavaBeans();
                    // es necesario que este en la misma orden que la tabla 
                    mjb.setId(rs.getString(1));// id
                    mjb.setNombre(rs.getString(2)); // nombre
                    mjb.setDni(rs.getString(3)); // direccion
                    mjb.setDireccion(rs.getString(4)); // direccion
                    mjb.setIngreso(rs.getString(5)); // ingreso
                    mjb.setPassword(rs.getString(6)); // password
                    mjb.setEstado(rs.getString(7)); // estado
                    resultado.add(mjb);
                }

                stmt.close();
                con.close();
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }

        } else if (abm.equals("eliminarAdministrador")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                Statement stmt = con.createStatement();
                stmt.execute("Select * from administradores");

                ResultSet rs = stmt.getResultSet();
                while (rs.next()) {
                    ModeloABMJavaBeans mjb = new ModeloABMJavaBeans();
                    // es necesario que este en la misma orden que la tabla 
                    mjb.setId(rs.getString(1));// id
                    mjb.setNombre(rs.getString(2)); // nombre
                    mjb.setDni(rs.getString(3)); // direccion
                    mjb.setDireccion(rs.getString(4)); // direccion
                    mjb.setIngreso(rs.getString(5)); // ingreso
                    mjb.setPassword(rs.getString(6)); // password
                    mjb.setEstado(rs.getString(7)); // estado
                    resultado.add(mjb);
                }

                stmt.close();
                con.close();
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }

        } else if (abm.equals("eliminarRegistro")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                Statement statement = con.createStatement();
                //statement.executeUpdate("Insert into `usuarios` (`idUsuario`,  `nombre`, `tipo`, `password`, `estado`) values ('" + idUsuario + "', '" + nombre + "', '" + tipoUsuario + "', '" + password + "', 'Habilitado')");
                statement.executeUpdate("delete from `administradores` where `id`='" + id + "'");

                statement.close();
                con.close();

            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        } else if (abm.equals("datosAdministrador")) {
            try {
                Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
                Statement stmt = con.createStatement();
                stmt.execute("Select * from administradores");

                ResultSet rs = stmt.getResultSet();
                while (rs.next()) {
                    ModeloABMJavaBeans mjb = new ModeloABMJavaBeans();
                    // es necesario que este en la misma orden que la tabla 
                    mjb.setId(rs.getString(1));// id
                    mjb.setNombre(rs.getString(2)); // nombre
                    mjb.setDni(rs.getString(3)); // direccion
                    mjb.setDireccion(rs.getString(4)); // direccion
                    mjb.setIngreso(rs.getString(5)); // ingreso
                    mjb.setPassword(rs.getString(6)); // password
                    mjb.setEstado(rs.getString(7)); // estado
                    resultado.add(mjb);
                }

                stmt.close();
                con.close();
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        }
        return true;
    }

    /*
     *  
     * 
     *   
     */
    public ArrayList<ModeloABMJavaBeans> getResultado() {
        return resultado;
    }

    private void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }

    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }

}
