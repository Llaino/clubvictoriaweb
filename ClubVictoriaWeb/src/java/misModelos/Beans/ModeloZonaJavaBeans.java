/**
 * Habilita la serialización de objetos en java, haciendo que la clase
 * ModeloJavaBeans herede de la clase java.io.Serializable.
 *
 * La serialización de objetos en Java evita tener que crear este archivo de
 * texto para almacenar los datos, y ahorrar tiempo así como también costos
 * de programación.
 */
/**
 *Se trata de una clase seriarizable que contiene unas variables que van a
 * almacenar la información que necesitamos. Éstas variables deben ser privadas.
 * Y para acceder a éstas variables deben implementarse unos metodos
 * get (para obtener el valor) y set (para establecer el valor).
 * Éstos métodos deben ser públicos, y debe tener también un constructor público.
 *
 */
package misModelos.Beans;

public class ModeloZonaJavaBeans implements java.io.Serializable {

    // Administracion Zonas
    private String id;
    private String letra; // letra asignada a la zona
    private String tipo; //  tipo de barco
    private String cantidad; // cantidad de barcos
    private String profundidad; // profundiad amarres
    private String ancho; // ancho amarres
    private String estado; // estado de la zona

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLetra() {
        return letra;
    }

    public void setLetra(String letra) {
        this.letra = letra;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getProfundidad() {
        return profundidad;
    }

    public void setProfundidad(String profundidad) {
        this.profundidad = profundidad;
    }

    public String getAncho() {
        return ancho;
    }

    public void setAncho(String ancho) {
        this.ancho = ancho;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
