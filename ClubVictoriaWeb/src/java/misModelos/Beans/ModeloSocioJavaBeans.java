/**
 * Habilita la serialización de objetos en java, haciendo que la clase
 * ModeloJavaBeans herede de la clase java.io.Serializable.
 *
 * La serialización de objetos en Java evita tener que crear este archivo de
 * texto para almacenar los datos, y ahorrar tiempo así como también costos
 * de programación.
 */
/**
 *Se trata de una clase seriarizable que contiene unas variables que van a
 * almacenar la información que necesitamos. Éstas variables deben ser privadas.
 * Y para acceder a éstas variables deben implementarse unos metodos
 * get (para obtener el valor) y set (para establecer el valor).
 * Éstos métodos deben ser públicos, y debe tener también un constructor público.
 *
 */
package misModelos.Beans;

public class ModeloSocioJavaBeans implements java.io.Serializable {

    private String id;
    private String idUsuario; // RELACION TABLA DE USUARIOS CON LA TABLA SOCIOS
    private String nombre;
    private String tipo; // TIPO DE USUARIO
    private String dni;
    private String direccion;
    private String telefono;
    private String ingreso;
    private String password;
    private String estado; // HABILITADO/ DESHABILITADO

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getIngreso() {
        return ingreso;
    }

    public void setIngreso(String ingreso) {
        this.ingreso = ingreso;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
