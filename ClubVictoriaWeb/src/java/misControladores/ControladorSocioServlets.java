/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misControladores;
import java.io.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import misModelos.Consultas.*;

public class ControladorSocioServlets extends HttpServlet { //HttpServlet importante

    private HttpServletRequest request;
    private HttpServletResponse response;
    private String opcion; // VARIABLE QUE PERMITIRA INGRESAR AL SWITCH / <C:CHOOSE> CORRESPONDIENTES
    private String id;
    private String idUsuario; // RELACION TABLA USUARIOS CON LA TABLA SOCIOS
    private String nombre;
    private String tipo; // TIPO DE USUARIO
    private String dni;
    private String direccion;
    private String telefono;
    private String ingreso;
    private String password;
    private String estado; // Habilitado/Deshabilitado
    // VARIABLE DONDE VOY A GUARDAR EL RESULTADO DE INGRESAR NUEVO REGISTRO.
    boolean consulta;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        this.request = request;
        this.response = response;

        // OBTENGO DATOS DE LA SOLICITUD REQUEST
        String ip = request.getParameter("dirIP");
        String bd = request.getParameter("nomBD");

        // OBTENGO DATOS DE LA SOLICITUD REQUEST
        this.opcion = request.getParameter("abm");

        try {
            this.id = request.getParameter("id");
        } catch (Exception e) {

        }
        try {
            this.idUsuario = request.getParameter("idUsuario");
        } catch (Exception e) {

        }
        try {
            this.nombre = request.getParameter("nombre");
        } catch (Exception e) {

        }
        try {
            this.dni = request.getParameter("dni");
        } catch (Exception e) {

        }
        try {
            this.direccion = request.getParameter("direccion");
        } catch (Exception e) {

        }
        try {
            this.telefono = request.getParameter("telefono");
        } catch (Exception e) {

        }
        try {
            this.ingreso = request.getParameter("ingreso");
        } catch (Exception e) {

        }
        try {
            this.password = request.getParameter("password");
        } catch (Exception e) {

        }

        try {
            this.estado = request.getParameter("estado");
        } catch (Exception e) {

        }

        // DECLARO UN OBJETO DE TIPO MODELO PARA PODER LLEGAR A SUS METODOS EN EL MOMENTO CUANDO SEA NECESARIO
        ModeloSocioPOJOs ms = new ModeloSocioPOJOs(ip, bd);

        /*
        * SWITCH TAREAS DEL ADMINISTRADOR SOBRE EL TIPO DE USUARIO SOCIO
        * 
        *   
         */
        switch (opcion) {
            case "ingresarSocio":
                
                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("abm", opcion);

                // ENVIO EL CONTENIDO DE LA VARIABLE "OPCION" HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                // SE CARGA LA VISTA ADMINISTRADOR CON LAS OPCIONES EN FORMATO DE FORMULARIO PARA INGRESAR LOS DATOS
                RequestDispatcher vm = request.getRequestDispatcher("vistaAdministrador.jsp");
                vm.forward(request, response);
                break;
            case "nuevoRegistroSocio":

                try {

                    // SI NO SE REALIZA LA INSERCIÓN, DEVOLVERA FALSE
                    consulta = ms.consultarAdministracionSocio(opcion, "", nombre, dni, direccion, telefono, ingreso, password, estado);

                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (consulta) { // CONSULTA TRUE, SIGNIFICA QUE LA OPERACIÓN NUEVO REGISTRO SE REALIZO

                    // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                    request.setAttribute("abm", opcion);

                    // ENVIO EL CONTENIDO DE LA VARIABLE "OPCION" HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                    // SE CARGA LA VISTA ADMINISTRADOR CON LAS OPCIONES EN FORMATO DE FORMULARIO PARA INGRESAR LOS DATOS
                    RequestDispatcher vistaNuevoRegistro = request.getRequestDispatcher("vistaAdministrador.jsp");
                    vistaNuevoRegistro.forward(request, response);

                } else { // CONSULTA FALSE, SIGNIFICA QUE LA OPERACIÓN NO SE REALIZO

                    // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                    request.setAttribute("error", "No se pudo realizar la operación <p></p>El nombre del usuario ya existe.");

                    RequestDispatcher vistaNuevoRegistro = request.getRequestDispatcher("vistaError.jsp");
                    vistaNuevoRegistro.forward(request, response);
                }
                break;
            case "modificarSocio":
                try {
                    consulta = ms.consultarAdministracionSocio(opcion, "", "", "", "", "", "", "", "");
                    //mp.addExceptionListener(new ExceptionListener());
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }

                // LLAMO AL METODO GETRESULTADO() PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("resultadosABM", ms.getResultado());

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("abm", opcion);

                // ENVIO EL CONTENIDO DE LA VARIABLE "OPCION" HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                // SE CARGA LA VISTA ADMINISTRADOR CON LAS OPCIONES EN FORMATO DE FORMULARIO PARA INGRESAR LOS DATOS
                RequestDispatcher vma = request.getRequestDispatcher("vistaAdministrador.jsp");
                vma.forward(request, response);
                break;
            case "modificarRegistroSocio":
                try {
                    consulta = ms.consultarAdministracionSocio(opcion, id, nombre, dni, direccion, telefono, ingreso, password, estado);
                    //mp.addExceptionListener(new ExceptionListener());
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (consulta) {
                    // LLAMO AL METODO GETRESULTADO() PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                    // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                    request.setAttribute("resultadosABM", ms.getResultado());

                    // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                    request.setAttribute("abm", opcion);

                    // ENVIO EL CONTENIDO DE LA VARIABLE "OPCION" HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                    // SE CARGA LA VISTA ADMINISTRADOR CON LAS OPCIONES EN FORMATO DE FORMULARIO PARA INGRESAR LOS DATOS
                    RequestDispatcher vmr = request.getRequestDispatcher("vistaAdministrador.jsp");
                    vmr.forward(request, response);
                } else { // CONSULTA FALSE, SIGNIFICA QUE LA OPERACIÓN NO SE REALIZO

                    // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                    request.setAttribute("error", "No se pudo realizar la operación <p></p>El nombre del usuario ya existe.");

                    RequestDispatcher vistaNuevoRegistro = request.getRequestDispatcher("vistaError.jsp");
                    vistaNuevoRegistro.forward(request, response);
                }
                break;
            case "eliminarSocio":
                try {

                    consulta = ms.consultarAdministracionSocio(opcion, "", "", "", "", "", "", "", "");
                    //mp.addExceptionListener(new ExceptionListener());
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }

                // LLAMO AL METODO GETRESULTADO() PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("resultadosABM", ms.getResultado());

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("abm", opcion);

                // ENVIO EL CONTENIDO DE LA VARIABLE "OPCION" HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                // SE CARGA LA VISTA ADMINISTRADOR CON LAS OPCIONES EN FORMATO DE FORMULARIO PARA INGRESAR LOS DATOS
                RequestDispatcher ve = request.getRequestDispatcher("vistaAdministrador.jsp");
                ve.forward(request, response);
                break;
            case "eliminarRegistroSocio":
                try {
                    // 2do
                    // ---- ModeloPojos consulta a la database ----
                    consulta = ms.consultarAdministracionSocio(opcion, id, "", "", "", "", "", "", "");
                    //mp.addExceptionListener(new ExceptionListener());
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }

                // LLAMO AL METODO GETRESULTADO() PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("resultadosABM", ms.getResultado());

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("abm", opcion);

                // ENVIO EL CONTENIDO DE LA VARIABLE "OPCION" HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                // SE CARGA LA VISTA ADMINISTRADOR CON LAS OPCIONES EN FORMATO DE FORMULARIO PARA INGRESAR LOS DATOS
                RequestDispatcher ver = request.getRequestDispatcher("vistaAdministrador.jsp");
                ver.forward(request, response);
                break;
            case "datosSocio":
                try {
                    // 2do
                    // ---- ModeloPojos consulta a la database ----
                    consulta = ms.consultarAdministracionSocio(opcion, "", "", "", "", "", "", "", "");
                    //mp.addExceptionListener(new ExceptionListener());
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }

                // LLAMO AL METODO GETRESULTADO() PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("resultadosABM", ms.getResultado());

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("abm", opcion);

                // ENVIO EL CONTENIDO DE LA VARIABLE "OPCION" HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                RequestDispatcher vem = request.getRequestDispatcher("vistaAdministrador.jsp");
                vem.forward(request, response);
                break;
            default:
                request.setAttribute("error", opcion);
                RequestDispatcher verror = request.getRequestDispatcher("vistaError.jsp");
                verror.forward(request, response);
                break;
        }
    }

    private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String exception = event.getActionCommand();
            request.setAttribute("mensajeError", exception);
            RequestDispatcher vista = request.getRequestDispatcher("vistaError.jsp");
            try {
                vista.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ControladorLoginServlets.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ControladorLoginServlets.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
