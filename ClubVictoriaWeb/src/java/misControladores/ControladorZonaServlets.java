/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misControladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import misModelos.Consultas.*;

public class ControladorZonaServlets extends HttpServlet { //HttpServlet importante

    private HttpServletRequest request;
    private HttpServletResponse response;
    private String opcion;
    private String id;
    private String letra;       // LETRA ASIGNADA A LA ZONA
    private String tipo;        // TIPO DE BARCO
    private String cantidad;    // CANTIDAD DE BARCOS POR ZONA
    private String profundidad; // PROFUNDIDAD DEL AMARRE EN METROS
    private String ancho;       // ANCHO DE AMARRE EN METROS
    private String estado;      // HABILITADA/ DESHABILITADA
    // VARIABLE DONDE VOY A GUARDAR EL RESULTADO DE INGRESAR NUEVO REGISTRO.
    boolean consulta;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        this.request = request;
        this.response = response;

        // OBTENGO DATOS DE SOLICITUD REQUEST
        String ip = request.getParameter("dirIP");
        String bd = request.getParameter("nomBD");

        // OBTENGO DATOS DE SOLICITUD REQUEST
        this.opcion = request.getParameter("abm");

        try {
            this.id = request.getParameter("id");
        } catch (Exception e) {

        }
        try {
            this.letra = request.getParameter("letra");
        } catch (Exception e) {

        }

        try {
            this.tipo = request.getParameter("tipo");
        } catch (Exception e) {

        }
        try {
            this.cantidad = request.getParameter("cantidad");
        } catch (Exception e) {

        }
        try {
            this.profundidad = request.getParameter("profundidad");
        } catch (Exception e) {

        }
        try {
            this.ancho = request.getParameter("ancho");
        } catch (Exception e) {

        }
        try {
            this.estado = request.getParameter("estado");
        } catch (Exception e) {

        }

        // DECLARO UN OBJETO DE TIPO MODELO PARA PODER LLEGAR A SUS METODOS EN EL MOMENTO CUANDO SEA NECESARIO
        ModeloZonaPOJOs mz = new ModeloZonaPOJOs(ip, bd);

        /*
        * SWITCH TAREAS DEL ADMINISTRADOR SOBRE EL TIPO DE USUARIO ZONA
        * 
        *   
         */
        switch (opcion) {
            case "ingresarZona":
                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("abm", opcion);

                // ENVIO EL CONTENIDO DE LA VARIABLE "OPCION" HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                // SE CARGA LA VISTA ADMINISTRADOR CON LAS OPCIONES EN FORMATO DE FORMULARIO PARA INGRESAR LOS DATOS
                RequestDispatcher vm = request.getRequestDispatcher("vistaAdministrador.jsp");
                vm.forward(request, response);
                break;
            case "nuevoRegistroZona":
                try {
                    consulta = mz.consultarAdministracionZona(opcion, "", letra, tipo, cantidad, profundidad, ancho, estado);

                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (consulta) { // CONSULTA TRUE, SIGNIFICA QUE LA OPERACIÓN NUEVO REGISTRO SE REALIZO

                    // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                    request.setAttribute("abm", opcion);

                    // ENVIO EL CONTENIDO DE LA VARIABLE "OPCION" HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                    // SE CARGA LA VISTA ADMINISTRADOR CON LAS OPCIONES EN FORMATO DE FORMULARIO PARA INGRESAR LOS DATOS
                    RequestDispatcher vistaNuevoRegistro = request.getRequestDispatcher("vistaAdministrador.jsp");
                    vistaNuevoRegistro.forward(request, response);

                } else { // CONSULTA FALSE, SIGNIFICA QUE LA OPERACIÓN NO SE REALIZO

                    // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                    request.setAttribute("error", "No se pudo realizar la operación <p></p>La letra identificadora ya existe.");

                    RequestDispatcher vistaNuevoRegistro = request.getRequestDispatcher("vistaError.jsp");
                    vistaNuevoRegistro.forward(request, response);
                }
                break;
            case "modificarZona":
                try {
                    consulta = mz.consultarAdministracionZona(opcion, "", "", "", "", "", "", "");

                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }

                // LLAMO AL METODO GETRESULTADO() PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("resultadosABM", mz.getResultado());

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("abm", opcion);

                // ENVIO EL CONTENIDO DE LA VARIABLE "OPCION" HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                // SE CARGA LA VISTA ADMINISTRADOR CON LAS OPCIONES EN FORMATO DE FORMULARIO PARA INGRESAR LOS DATOS
                RequestDispatcher vma = request.getRequestDispatcher("vistaAdministrador.jsp");
                vma.forward(request, response);
                break;
            case "modificarRegistroZona":
                try {
                    // 2do
                    // ---- ModeloPojos consulta a la database ----
                    consulta = mz.consultarAdministracionZona(opcion, id, letra, tipo, cantidad, profundidad, ancho, estado);
                    //mp.addExceptionListener(new ExceptionListener());
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (consulta) { // CONSULTA TRUE, SIGNIFICA QUE LA OPERACIÓN UPDATE SE REALIZO

                    // LLAMO AL METODO GETRESULTADO() PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                    // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                    request.setAttribute("resultadosABM", mz.getResultado());

                    // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                    request.setAttribute("abm", opcion);

                    // ENVIO EL CONTENIDO DE LA VARIABLE "OPCION" HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                    // SE CARGA LA VISTA ADMINISTRADOR CON LAS OPCIONES EN FORMATO DE FORMULARIO PARA INGRESAR LOS DATOS
                    RequestDispatcher vmr = request.getRequestDispatcher("vistaAdministrador.jsp");
                    vmr.forward(request, response);
                } else { // CONSULTA FALSE, SIGNIFICA QUE LA OPERACIÓN NO SE REALIZO

                    // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                    request.setAttribute("error", "No se pudo realizar la operación <p></p>La letra identificadora ya existe.");

                    RequestDispatcher vistaNuevoRegistro = request.getRequestDispatcher("vistaError.jsp");
                    vistaNuevoRegistro.forward(request, response);
                }
                break;
            case "eliminarZona":
                try {
                    // 2do
                    // ---- ModeloPojos consulta a la database ----
                    consulta = mz.consultarAdministracionZona(opcion, "", "", "", "", "", "", "");
                    //mp.addExceptionListener(new ExceptionListener());
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }

                // LLAMO AL METODO GETRESULTADO() PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("resultadosABM", mz.getResultado());

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("abm", opcion);

                // ENVIO EL CONTENIDO DE LA VARIABLE "OPCION" HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                // SE CARGA LA VISTA ADMINISTRADOR CON LAS OPCIONES EN FORMATO DE FORMULARIO PARA INGRESAR LOS DATOS
                RequestDispatcher ve = request.getRequestDispatcher("vistaAdministrador.jsp");
                ve.forward(request, response);
                break;
            case "eliminarRegistroZona":
                try {
                    // 2do
                    // ---- ModeloPojos consulta a la database ----
                    consulta = mz.consultarAdministracionZona(opcion, id, "", "", "", "", "", "");
                    //mp.addExceptionListener(new ExceptionListener());
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }

                // LLAMO AL METODO GETRESULTADO() PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("resultadosABM", mz.getResultado());

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("abm", opcion);

                // ENVIO EL CONTENIDO DE LA VARIABLE "OPCION" HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                // SE CARGA LA VISTA ADMINISTRADOR CON LAS OPCIONES EN FORMATO DE FORMULARIO PARA INGRESAR LOS DATOS
                RequestDispatcher ver = request.getRequestDispatcher("vistaAdministrador.jsp");
                ver.forward(request, response);
                break;
            case "datosZona":
                try {
                    // 2do
                    // ---- ModeloPojos consulta a la database ----
                    consulta = mz.consultarAdministracionZona(opcion, "", "", "", "", "", "", "");
                    //mp.addExceptionListener(new ExceptionListener());
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }

                // LLAMO AL METODO GETRESULTADO() PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("resultadosABM", mz.getResultado());

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("abm", opcion);

                // ENVIO EL CONTENIDO DE LA VARIABLE "OPCION" HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                RequestDispatcher vem = request.getRequestDispatcher("vistaAdministrador.jsp");
                vem.forward(request, response);
                break;
            default:
                request.setAttribute("error", opcion);
                RequestDispatcher verror = request.getRequestDispatcher("vistaError.jsp");
                verror.forward(request, response);
                break;
        }
    }

    private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String exception = event.getActionCommand();
            request.setAttribute("mensajeError", exception);
            RequestDispatcher vista = request.getRequestDispatcher("vistaError.jsp");
            try {
                vista.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ControladorLoginServlets.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ControladorLoginServlets.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
