/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misControladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import misModelos.Consultas.*;

public class ControladorLoginServlets extends HttpServlet { //HttpServlet importante

    private HttpServletRequest request;
    private HttpServletResponse response;
    private String nombre;
    private String password;
    private String tipo;

    public ControladorLoginServlets() {
        this.tipo = "";
        this.nombre = "";
        this.password = "";
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        // RECIBE DATOS DE INDEX.JSP
        this.request = request;
        this.response = response;

        // OBTENGO DATOS DE SOLICITUD REQUEST
        String ip = request.getParameter("dirIP");
        String bd = request.getParameter("nomBD");

        // ASIGNO VALOR A LAS VARIABLES DEL CONTROLADOR LOGIN
        this.nombre = request.getParameter("nombre");
        this.password = request.getParameter("password");

        // DECLARO OBJETO DE TIPO MODELO ABM PARA USAR SUS METODOS CUANDO SEA NECEARIO
        ModeloABMPOJOs mp = new ModeloABMPOJOs(ip, bd);

        // EJECUTO METODOS PERTENECIENTES AL MODELO ABM
        // CONSULTO SI EXISTE EL NOMBRE Y LA CONTRASEÑA
        // OBTENGO EL TIPO DE USUARIO
        tipo = mp.consultarLogin(nombre, password); // NOMBRE Y CONTRASEÑA PARA INGRESAR

        // SEGUN EL TIPO DE USUARIO, SE MOSTRARA LA VISTA AL CLIENTE
        switch (tipo) {
            case "Socio":

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA
                request.setAttribute("resultadosLogin", mp.getResultado());
                request.setAttribute("nombre", nombre);

                // ENSEÑO LOS DATOS EN FORMATO DE VISTA
                RequestDispatcher vs = request.getRequestDispatcher("vistaSocio.jsp");
                vs.forward(request, response);

                break;
            case "Empleado":

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA
                request.setAttribute("resultadosLogin", mp.getResultado());
                request.setAttribute("nombre", nombre);

                // ENSEÑO LOS DATOS EN FORMATO DE VISTA
                RequestDispatcher ve = request.getRequestDispatcher("vistaEmpleado.jsp");
                ve.forward(request, response);

                break;
            case "Administrador":

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA
                request.setAttribute("resultadosLogin", mp.getResultado());
                request.setAttribute("nombre", nombre);

                // ENSEÑO LOS DATOS EN FORMATO DE VISTA
                RequestDispatcher va = request.getRequestDispatcher("vistaAdministrador.jsp");
                va.forward(request, response);
                break;
            case "Nombre o contraseña no corresponden":

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA
                request.setAttribute("error",  "Nombre o contraseña no corresponden");

                // ENSEÑO LOS DATOS EN FORMATO DE VISTA
                RequestDispatcher vn = request.getRequestDispatcher("vistaError.jsp");
                vn.forward(request, response);
                break;
            case "Usuario Deshabilitado":

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA
                request.setAttribute("error", nombre + " Usuario Deshabilitado");

                // ENSEÑO LOS DATOS EN FORMATO DE VISTA
                RequestDispatcher vd = request.getRequestDispatcher("vistaError.jsp");
                vd.forward(request, response);
                break;
            default:
                request.setAttribute("error", tipo);
                RequestDispatcher verror = request.getRequestDispatcher("vistaError.jsp");
                verror.forward(request, response);
                break;
        }

    }

    private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String exception = event.getActionCommand();
            request.setAttribute("Mensaje Error", exception);
            RequestDispatcher vista = request.getRequestDispatcher("vistaError.jsp");
            try {
                vista.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ControladorLoginServlets.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ControladorLoginServlets.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
