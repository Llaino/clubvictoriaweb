/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misControladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import misModelos.Consultas.*;

public class ControladorABMServlets extends HttpServlet { //HttpServlet importante

    private HttpServletRequest request;
    private HttpServletResponse response;
    private String opcion;
    private String id;
    private String nombre;
    private String tipo;
    private String dni;
    private String direccion;
    private String ingreso;
    private String password;
    private String estado;
    // VARIABLE DONDE VOY A GUARDAR EL RESULTADO DE INGRESAR NUEVO REGISTRO.
    boolean consulta;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        this.request = request;
        this.response = response;

        // OBTENGO DATOS DE SOLICITUD REQUEST
        String ip = request.getParameter("dirIP");
        String bd = request.getParameter("nomBD");

        // OBTENGO DATOS DE SOLICITUD REQUEST
        this.opcion = request.getParameter("abm");

        try {
            this.id = request.getParameter("id");
        } catch (Exception e) {

        }
        try {
            this.nombre = request.getParameter("nombre");
        } catch (Exception e) {

        }
        try {
            this.tipo = request.getParameter("tipo");
        } catch (Exception e) {

        }
        try {
            this.dni = request.getParameter("dni");
        } catch (Exception e) {

        }
        try {
            this.direccion = request.getParameter("direccion");
        } catch (Exception e) {

        }
        try {
            this.ingreso = request.getParameter("ingreso");
        } catch (Exception e) {

        }
        try {
            this.password = request.getParameter("password");
        } catch (Exception e) {

        }

        try {
            this.estado = request.getParameter("estado");
        } catch (Exception e) {

        }

        // DECLARO OBJETO DE TIPO MODELO ABM, PARA USAR SUS METODOS CUANDO SEA NECESARIO
        ModeloABMPOJOs mp = new ModeloABMPOJOs(ip, bd);

        /*
        * SWITCH TAREAS DEL ADMINISTRADOR SOBRE EL TIPO DE USUARIO ADMINISTRADOR
        * 
        *   
         */
        switch (opcion) {
            case "socio":
            case "empleado":
            case "zona":
            case "amarre":
            case "embarcacion":
            case "administrador":

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("abm", opcion);

                // ENVIO LA RESPUESTA HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                RequestDispatcher vz = request.getRequestDispatcher("vistaAdministrador.jsp");
                vz.forward(request, response);
                break;

            case "ingresarAdministrador":
                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("abm", opcion);

                // ENVIO EL CONTENIDO DE LA VARIABLE "OPCION" HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                // SE CARGA LA VISTA ADMINISTRADOR CON LAS OPCIONES EN FORMATO DE FORMULARIO PARA INGRESAR LOS DATOS
                RequestDispatcher vm = request.getRequestDispatcher("vistaAdministrador.jsp");
                vm.forward(request, response);
                break;

            // INGRESO NUEVO REGISTRO CON LOS DATOS DEL NUEVO ADMINISTRADOR
            case "nuevoRegistro":
                try {
                    //LLAMO AL METODO CONSULTAR PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                    consulta = mp.consultarAdministracionAdministrador(opcion, "", nombre, dni, direccion, ingreso, password, estado);

                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                    System.err.println(ex.getMessage());
                }
                if (consulta) { // CONSULTA TRUE, SIGNIFICA QUE LA OPERACIÓN NUEVO REGISTRO SE REALIZO

                    // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                    request.setAttribute("abm", opcion);

                    // ENVIO EL CONTENIDO DE LA VARIABLE "OPCION" HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                    // SE CARGA LA VISTA ADMINISTRADOR CON LAS OPCIONES EN FORMATO DE FORMULARIO PARA INGRESAR LOS DATOS
                    RequestDispatcher vistaNuevoRegistro = request.getRequestDispatcher("vistaAdministrador.jsp");
                    vistaNuevoRegistro.forward(request, response);

                } else { // CONSULTA FALSE, SIGNIFICA QUE LA OPERACIÓN NO SE REALIZO

                    // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                    request.setAttribute("nombre", "No se pudo realizar la operación <p></p>La letra identificadora ya existe.");

                    RequestDispatcher vistaNuevoRegistro = request.getRequestDispatcher("vistaError.jsp");
                    vistaNuevoRegistro.forward(request, response);
                }
                break;

            case "modificarAdministrador":
                try {
                    //LLAMO AL METODO CONSULTAR PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                    consulta = mp.consultarAdministracionAdministrador(opcion, "", "", "", "", "", "", "");
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }

                // LLAMO AL METODO GETRESULTADO() PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("resultadosABM", mp.getResultado());

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("abm", opcion);

                // ENVIO LA RESPUESTA HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                RequestDispatcher vma = request.getRequestDispatcher("vistaAdministrador.jsp");
                vma.forward(request, response);
                break;
            case "modificarRegistro":
                try {
                    //LLAMO AL METODO CONSULTAR PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                    consulta = mp.consultarAdministracionAdministrador(opcion, id, nombre, dni, direccion, ingreso, password, estado);
                    //mp.addExceptionListener(new ExceptionListener());
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (consulta) {
                    // LLAMO AL METODO GETRESULTADO() PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                    // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                    request.setAttribute("resultadosABM", mp.getResultado());

                    // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                    request.setAttribute("abm", opcion);

                    // ENVIO LA RESPUESTA HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                    RequestDispatcher vmr = request.getRequestDispatcher("vistaAdministrador.jsp");
                    vmr.forward(request, response);

                } else { // CONSULTA FALSE, SIGNIFICA QUE LA OPERACIÓN NO SE REALIZO

                    // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                    request.setAttribute("error", "No se pudo realizar la operación <p></p>El nombre del usuario ya existe.");

                    RequestDispatcher vistaNuevoRegistro = request.getRequestDispatcher("vistaError.jsp");
                    vistaNuevoRegistro.forward(request, response);
                }
                break;
            case "eliminarAdministrador":
                try {
                    //LLAMO AL METODO CONSULTAR PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                    consulta = mp.consultarAdministracionAdministrador(opcion, "", "", "", "", "", "", "");
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }

                // LLAMO AL METODO GETRESULTADO() PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("resultadosABM", mp.getResultado());

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("abm", opcion);

                // ENVIO LA RESPUESTA HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                RequestDispatcher ve = request.getRequestDispatcher("vistaAdministrador.jsp");
                ve.forward(request, response);
                break;
            case "eliminarRegistro":
                try {
                    //LLAMO AL METODO CONSULTAR PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                    consulta = mp.consultarAdministracionAdministrador(opcion, id, "", "", "", "", "", "");
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }

                // LLAMO AL METODO GETRESULTADO() PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("resultadosABM", mp.getResultado());

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA"
                request.setAttribute("abm", opcion);

                // ENVIO LA RESPUESTA HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                RequestDispatcher ver = request.getRequestDispatcher("vistaAdministrador.jsp");
                ver.forward(request, response);
                break;
            case "datosAdministrador":
                try {
                    //LLAMO AL METODO CONSULTAR PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                    consulta = mp.consultarAdministracionAdministrador(opcion, "", "", "", "", "", "", "");
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorABMServlets.class.getName()).log(Level.SEVERE, null, ex);
                }

                // LLAMO AL METODO GETRESULTADO() PERTENECIENTE A MODELO, CON ESO OBTENGO LOS DATOS DE LA BASE DE DATOS
                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("resultadosABM", mp.getResultado());

                // ARMO UNA RESPUESTA PARA QUE LA VISTA PUEDA INTERPRETARLA EN EL MOMENTO CUANDO LA RESPUESTA SEA ENVIADA
                request.setAttribute("abm", opcion);

                // ENVIO LA RESPUESTA HACIA LA VISTA , AHI ENTRA AL <C:CHOOSE> CORRESPONDIENTE
                RequestDispatcher vem = request.getRequestDispatcher("vistaAdministrador.jsp");
                vem.forward(request, response);
                break;
            case "ingresarSocio":
                request.setAttribute("error", "ingresar");
                RequestDispatcher verr = request.getRequestDispatcher("vistaError.jsp");
                verr.forward(request, response);
                break;
            default:
                request.setAttribute("error", opcion);
                RequestDispatcher verror = request.getRequestDispatcher("vistaError.jsp");
                verror.forward(request, response);
                break;
        } // TERMINA SWITCH TAREAS DEL ADMINISTRADOR
    }

    private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String exception = event.getActionCommand();
            request.setAttribute("mensajeError", exception);
            RequestDispatcher vista = request.getRequestDispatcher("vistaError.jsp");
            try {
                vista.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ControladorLoginServlets.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ControladorLoginServlets.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
