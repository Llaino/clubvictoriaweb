<%-- 
    Document   : vista
    Created on : Oct 13, 2019, 7:49:56 PM
    Author     : vivi
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>



<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>vistaAdministradorJSP</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no,text/html; charset=UTF-8" />
        <link rel="stylesheet" href="editorial/assets/css/main.css" />
    </head>

    <body>
        <%-- 
        ----------------- Boton Salir -----------------
        <a class="boton-personalizado-5" href="http://localhost:8080/ClubVictoriaWeb/">Salir</a> 
        --%>

        <%-- 
        ----------------- Login ----------------- 
        --%>
        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->
            <div id="main">
                <div class="inner">
                    <c:choose>
                        <c:when test="${empty nombre}">
                            <header id="header">
                                <a class="logo"><strong>Administración</strong></a>
                            </header>
                        </c:when> 
                        <c:otherwise>
                            <header id="header">
                                <a class="logo"><strong>Administrador:</strong> ${nombre}</a>
                            </header>
                        </c:otherwise>
                    </c:choose>


                    <!-- Content -->
                    <c:choose>
                        <c:when test="${empty nombre}">
                        </c:when> 
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${empty resultadosLogin}">
                                    <h1>Error. No hay resultados Login. Verificar conexión a base de datos.</h1>
                                </c:when>
                                <c:otherwise>
                                    <font color="gray" size="5">Administrador: ${nombre}</font>
                                    <table class="egt">
                                        <tr>
                                            <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal">Id</font></td>
                                            <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray" >idUsuario</font> </td>
                                            <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray" >Nombre</font> </td>
                                            <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Tipo</font></td>
                                            <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Contraseña</font></td>
                                            <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Estado</font> </td>
                                        </tr>
                                        <c:forEach var="resultados" items="${resultadosLogin}">
                                            <tr>
                                                <td><font color="teal">${resultados.id} </font></td>
                                                <td><font color="teal">${resultados.idUsuario} </font></td>
                                                <td><font color="gray">${resultados.nombre} </font> </td>
                                                <td><font color="gray">${resultados.tipo} </font></td>
                                                <td><font color="gray">${resultados.password} </font></td>
                                                    <c:choose>
                                                        <c:when test="${resultados.estado=='Habilitado'}">
                                                        <td><font color="gray">${resultados.estado} </font> </td>
                                                        </c:when>
                                                        <c:otherwise>
                                                        <td><font color="red">${resultados.estado} </font> </td>
                                                        </c:otherwise>
                                                    </c:choose>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                    <%--  
                                    <form method="post" action="ControladorABMServlets">
                                        <font color="gray">Seleccionar para administrar</font>
                                        <select name="abm" >
                                            <option value="socio">Socios</option>
                                            <option value="empleado">Empleados</option>
                                            <option value="embarcacion" disabled="disabled" >Embarcaciones</option>
                                            <option value="amarre" disabled="disabled" >Amarres</option>
                                            <option value="zona">Zonas</option>
                                            <option value="administrador">Administrador</option>
                                        </select>

                                        <input type="hidden" name="dirIP" value="localhost">
                                        <input type="hidden" name="nomBD" value="databaseclub">
                                        <input type="submit"  value="Solicitud">
                                    </form>    
                                    --%>
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                    <%-- 
                    ----------------- Tareas de administración ----------------- 
                    --%>
                    <section>
                        <c:choose>
                            <c:when test="${empty abm}">
                            </c:when>
                            <c:otherwise>
                                <p></p>
                                <c:choose>
                                    <%-- 
                                    Administración Socios
                                    --%>
                                    <c:when test="${abm=='socio' or abm=='nuevoRegistroSocio' or abm=='modificarRegistroSocio' or abm=='eliminarRegistroSocio'}">
                                        <c:choose>
                                            <c:when test="${abm=='socio'}">
                                                <font color="gray" size="5">${abm}</font>
                                            </c:when>
                                            <c:otherwise>
                                                <font color="gray" size="5">${abm} se realizo correctamente!</font>
                                            </c:otherwise>
                                        </c:choose>
                                        <%-- 
                                <form method="post" action="ControladorSocioServlets">
                                    <select name="abm" >
                                        <option value="ingresarSocio">Ingresar Socio</option>
                                        <option value="modificarSocio">Modificar Socio</option>
                                        <option value="eliminarSocio">Eliminar Socio</option>
                                        <option value="datosSocio">Vista datos Socios</option>
                                    </select>

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">
                                            <input type="submit"  value="Solicitud">
                                        </form>    
                                        --%>
                                    </c:when>
                                    <c:when test="${abm=='ingresarSocio'}">
                                        <form method="post" action="ControladorSocioServlets">
                                            <font color="gray" size="5">${abm}</font>
                                            <input type="text" name="nombre" placeholder="Nombre">
                                            <input type="text" name="dni" placeholder="DNI" >
                                            <input type="text" name="direccion" placeholder="Direccion" >
                                            <input type="text" name="telefono" placeholder="Telefono" >
                                            <input type="text" name="ingreso" placeholder="dd-mm-aaaa" >
                                            <input type="text" name="password" placeholder="Contraseña" >
                                            <select name="estado" >
                                                <option value="Habilitado">Habilitado</option>
                                                <option value="Deshabilitado">Deshabilitado</option>
                                            </select>

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">

                                            <input type="hidden" name="abm" value="nuevoRegistroSocio">
                                            <input type="submit"  value="Solicitud">
                                        </form>    
                                    </c:when>
                                    <c:when test="${abm=='modificarSocio'}">
                                        <c:choose>
                                            <c:when test="${empty resultadosABM}">
                                                <p></p>
                                                <p></p>
                                                <h1>ResultadosABM vacio</h1>
                                            </c:when>
                                            <c:otherwise>
                                                <p></p>
                                                <p></p>
                                                <table class="egt">
                                                    <tr>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal">Id</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray" >Nombre</font> </td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">DNI</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Direccion</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Telefono</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Ingreso</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Contraseña</font> </td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Estado</font> </td>
                                                    </tr>
                                                    <c:forEach var="resultados" items="${resultadosABM}">
                                                        <tr>
                                                            <td><font color="teal">${resultados.id} </font></td>
                                                            <td><font color="gray">${resultados.nombre} </font> </td>
                                                            <td><font color="gray">${resultados.dni} </font></td>
                                                            <td><font color="gray">${resultados.direccion} </font></td>
                                                            <td><font color="gray">${resultados.telefono} </font></td>
                                                            <td><font color="gray">${resultados.ingreso} </font></td>
                                                            <td><font color="gray">${resultados.password} </font></td>
                                                                <c:choose>
                                                                    <c:when test="${resultados.estado=='Habilitado'}">
                                                                    <td><font color="gray">${resultados.estado} </font> </td>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                    <td><font color="red">${resultados.estado} </font> </td>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                        </tr>
                                                    </c:forEach>
                                                </table>
                                                <p></p>
                                                <p></p>
                                            </c:otherwise>
                                        </c:choose>  
                                        <form method="post" action="ControladorSocioServlets">
                                            <font color="gray" size="5">${abm}</font>
                                            <input type="text" name="id" placeholder="ID">
                                            <input type="text" name="nombre" placeholder="Nombre">
                                            <input type="text" name="dni" placeholder="DNI" >
                                            <input type="text" name="direccion" placeholder="Direccion" >
                                            <input type="text" name="telefono" placeholder="Telefono" >
                                            <input type="text" name="ingreso" placeholder="dd-mm-aaaa" >
                                            <input type="text" name="password" placeholder="Contraseña" >
                                            <select name="estado" >
                                                <option value="Habilitado">Habilitado</option>
                                                <option value="Deshabilitado">Deshabilitado</option>
                                            </select>

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">

                                            <input type="hidden" name="abm" value="modificarRegistroSocio">
                                            <input type="submit"  value="Solicitud">  
                                        </form>
                                    </c:when>
                                    <c:when test="${abm=='eliminarSocio'}">
                                        <c:choose>
                                            <c:when test="${empty resultadosABM}">
                                                <p></p>
                                                <p></p>
                                                <h1>ResultadosABM vacio</h1>
                                            </c:when>
                                            <c:otherwise>
                                                <p></p>
                                                <p></p>
                                                <font color="gray" size="5">datosSocio</font>
                                                <p></p>
                                                <table class="egt">
                                                    <tr>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal">Id</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray" >Nombre</font> </td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">DNI</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Direccion</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Telefono</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Ingreso</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Contraseña</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Estado</font></td>
                                                    </tr>
                                                    <c:forEach var="resultados" items="${resultadosABM}">
                                                        <tr>
                                                            <td><font color="teal">${resultados.id} </font></td>
                                                            <td><font color="gray">${resultados.nombre} </font> </td>
                                                            <td><font color="gray">${resultados.dni} </font></td>
                                                            <td><font color="gray">${resultados.direccion} </font></td>
                                                            <td><font color="gray">${resultados.telefono} </font></td>
                                                            <td><font color="gray">${resultados.ingreso} </font></td>
                                                            <td><font color="gray">${resultados.password} </font></td>
                                                                <c:choose>
                                                                    <c:when test="${resultados.estado=='Habilitado'}">
                                                                    <td><font color="gray">${resultados.estado} </font> </td>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                    <td><font color="red">${resultados.estado} </font> </td>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                        </tr>
                                                    </c:forEach>
                                                </table>
                                                <p></p>
                                                <p></p>
                                            </c:otherwise>
                                        </c:choose>  
                                        <form method="post" action="ControladorSocioServlets">
                                            <p></p>
                                            <p></p>
                                            <font color="gray" size="5">${abm}</font>
                                            <p></p>
                                            <font color="gray">Seleccionar registro para eliminar </font>
                                            <input type="text" name="id" placeholder="ID">

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">

                                            <input type="hidden" name="abm" value="eliminarRegistroSocio">
                                            <input type="submit"  value="Solicitud">
                                        </form>   
                                    </c:when>
                                    <c:when test="${abm=='datosSocio'}">
                                        <c:choose>
                                            <c:when test="${empty resultadosABM}">
                                                <p></p>
                                                <p></p>
                                                <h1>ResultadosABM vacio</h1>
                                            </c:when>
                                            <c:otherwise>
                                                <p></p>
                                                <p></p>
                                                <font color="gray" size="5">${abm}</font>
                                                <p></p>
                                                <table class="egt">
                                                    <tr>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal">Id</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray" >Nombre</font> </td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">DNI</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Direccion</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Telefono</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Ingreso</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Contraseña</font> </td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Estado</font> </td>
                                                    </tr>
                                                    <c:forEach var="resultados" items="${resultadosABM}">
                                                        <tr>
                                                            <td><font color="teal">${resultados.id} </font></td>
                                                            <td><font color="gray">${resultados.nombre} </font> </td>
                                                            <td><font color="gray">${resultados.dni} </font></td>
                                                            <td><font color="gray">${resultados.direccion} </font></td>
                                                            <td><font color="gray">${resultados.telefono} </font></td>
                                                            <td><font color="gray">${resultados.ingreso} </font></td>
                                                            <td><font color="gray">${resultados.password} </font></td>
                                                                <c:choose>
                                                                    <c:when test="${resultados.estado=='Habilitado'}">
                                                                    <td><font color="gray">${resultados.estado} </font> </td>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                    <td><font color="red">${resultados.estado} </font> </td>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                        </tr>
                                                    </c:forEach>
                                                </table>
                                                <p></p>
                                                <p></p>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <%-- 
                                    **
                                    ************* Administración Empleados *************
                                    **
                                    --%>
                                    <c:when test="${abm=='empleado' or abm=='nuevoRegistroEmpleado' or abm=='modificarRegistroEmpleado' or abm=='eliminarRegistroEmpleado'}">
                                        <p></p>
                                        <c:choose>
                                            <c:when test="${abm=='empleado'}">
                                                <font color="gray" size="5">${abm}</font>
                                            </c:when>
                                            <c:otherwise>
                                                <font color="gray" size="5">${abm} se realizo correctamente!</font>
                                            </c:otherwise>
                                        </c:choose>
                                        <%--  
                               <form method="post" action="ControladorEmpleadoServlets">
                                   <select name="abm" >
                                       <option value="ingresarEmpleado">Ingresar Empleado</option>
                                       <option value="modificarEmpleado">Modificar Empleado</option>
                                       <option value="eliminarEmpleado">Eliminar Empleado</option>
                                       <option value="datosEmpleado">Vista datos Empleados</option>
                                   </select>

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">
                                            <input type="submit"  value="Solicitud">
                                        </form>    
                                        --%>
                                    </c:when>
                                    <c:when test="${abm=='ingresarEmpleado'}">
                                        <form method="post" action="ControladorEmpleadoServlets">
                                            <font color="gray" size="5">${abm}</font>
                                            <input type="text" name="codigo" placeholder="Codigo">
                                            <input type="text" name="nombre" placeholder="Nombre" >
                                            <input type="text" name="direccion" placeholder="Direccion" >
                                            <input type="text" name="telefono" placeholder="Telefono" >
                                            <input type="text" name="especialidad" placeholder="Especialidad" >
                                            <input type="text" name="password" placeholder="Contraseña" >
                                            <select name="estado" >
                                                <option value="Habilitado">Habilitado</option>
                                                <option value="Deshabilitado">Deshabilitado</option>
                                            </select>

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">

                                            <input type="hidden" name="abm" value="nuevoRegistroEmpleado">
                                            <input type="submit"  value="Solicitud">
                                        </form>    
                                    </c:when>
                                    <c:when test="${abm=='modificarEmpleado'}">
                                        <c:choose>
                                            <c:when test="${empty resultadosABM}">
                                                <p></p>
                                                <p></p>
                                                <h1>ResultadosABM vacio</h1>
                                            </c:when>
                                            <c:otherwise>
                                                <font color="gray" size="5">datosEmpleado</font>
                                                <table class="egt">
                                                    <tr>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal">Id</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray" >Codigo</font> </td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Nombre</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Direccion</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Telefono</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Especialidad</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Contraseña</font> </td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Estado</font> </td>
                                                    </tr>
                                                    <c:forEach var="resultados" items="${resultadosABM}">
                                                        <tr>
                                                            <td><font color="teal">${resultados.id} </font></td>
                                                            <td><font color="gray">${resultados.codigo} </font> </td>
                                                            <td><font color="gray">${resultados.nombre} </font></td>
                                                            <td><font color="gray">${resultados.direccion} </font></td>
                                                            <td><font color="gray">${resultados.telefono} </font></td>
                                                            <td><font color="gray">${resultados.especialidad} </font></td>
                                                            <td><font color="gray">${resultados.password} </font></td>
                                                                <c:choose>
                                                                    <c:when test="${resultados.estado=='Habilitado'}">
                                                                    <td><font color="gray">${resultados.estado} </font> </td>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                    <td><font color="red">${resultados.estado} </font> </td>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                        </tr>
                                                    </c:forEach>
                                                </table>
                                            </c:otherwise>
                                        </c:choose>  
                                        <form method="post" action="ControladorEmpleadoServlets">
                                            <p></p>
                                            <p></p>
                                            <font color="gray" size="5">${abm}</font>
                                            <p></p>
                                            <input type="text" name="id" placeholder="ID">
                                            <input type="text" name="codigo" placeholder="Nombre">
                                            <input type="text" name="nombre" placeholder="DNI" >
                                            <input type="text" name="direccion" placeholder="Direccion" >
                                            <input type="text" name="telefono" placeholder="Telefono" >
                                            <input type="text" name="especialidad" placeholder="Especialidad" >
                                            <input type="text" name="password" placeholder="Contraseña" >
                                            <select name="estado" >
                                                <option value="Habilitado">Habilitado</option>
                                                <option value="Deshabilitado">Deshabilitado</option>
                                            </select>

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">

                                            <input type="hidden" name="abm" value="modificarRegistroEmpleado">
                                            <input type="submit"  value="Solicitud">  
                                        </form>
                                    </c:when>
                                    <c:when test="${abm=='eliminarEmpleado'}">
                                        <c:choose>
                                            <c:when test="${empty resultadosABM}">
                                                <h1>ResultadosABM vacio</h1>
                                            </c:when>
                                            <c:otherwise>
                                                <font color="gray" size="5">datosEmpleado</font>
                                                <table class="egt">
                                                    <tr>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal">Id</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray" >Codigo</font> </td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Nombre</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Direccion</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Telefono</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Especialidad</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Contraseña</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Estado</font></td>
                                                    </tr>
                                                    <c:forEach var="resultados" items="${resultadosABM}">
                                                        <tr>
                                                            <td><font color="teal">${resultados.id} </font></td>
                                                            <td><font color="gray">${resultados.codigo} </font> </td>
                                                            <td><font color="gray">${resultados.nombre} </font></td>
                                                            <td><font color="gray">${resultados.direccion} </font></td>
                                                            <td><font color="gray">${resultados.telefono} </font></td>
                                                            <td><font color="gray">${resultados.especialidad} </font></td>
                                                            <td><font color="gray">${resultados.password} </font></td>
                                                                <c:choose>
                                                                    <c:when test="${resultados.estado=='Habilitado'}">
                                                                    <td><font color="gray">${resultados.estado} </font> </td>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                    <td><font color="red">${resultados.estado} </font> </td>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                        </tr>
                                                    </c:forEach>
                                                </table>
                                            </c:otherwise>
                                        </c:choose>  
                                        <form method="post" action="ControladorEmpleadoServlets">
                                            <font color="gray" size="5">${abm}</font>
                                            <font color="gray">Seleccionar registro para eliminar </font>
                                            <input type="text" name="id" placeholder="ID">

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">

                                            <input type="hidden" name="abm" value="eliminarRegistroEmpleado">
                                            <input type="submit"  value="Solicitud">
                                        </form>   
                                    </c:when>
                                    <c:when test="${abm=='datosEmpleado'}">
                                        <c:choose>
                                            <c:when test="${empty resultadosABM}">
                                                <h1>ResultadosABM vacio</h1>
                                            </c:when>
                                            <c:otherwise>
                                                <font color="gray" size="5">${abm}</font>
                                                <table class="egt">
                                                    <tr>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal">Id</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray" >Codigo</font> </td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Nombre</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Direccion</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Telefono</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Especialidad</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Contraseña</font> </td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Estado</font> </td>
                                                    </tr>
                                                    <c:forEach var="resultados" items="${resultadosABM}">
                                                        <tr>
                                                            <td><font color="teal">${resultados.id} </font></td>
                                                            <td><font color="gray">${resultados.codigo} </font> </td>
                                                            <td><font color="gray">${resultados.nombre} </font></td>
                                                            <td><font color="gray">${resultados.direccion} </font></td>
                                                            <td><font color="gray">${resultados.telefono} </font></td>
                                                            <td><font color="gray">${resultados.especialidad} </font></td>
                                                            <td><font color="gray">${resultados.password} </font></td>
                                                                <c:choose>
                                                                    <c:when test="${resultados.estado=='Habilitado'}">
                                                                    <td><font color="gray">${resultados.estado} </font> </td>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                    <td><font color="red">${resultados.estado} </font> </td>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                        </tr>
                                                    </c:forEach>
                                                </table>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <%--
                                    **
                                    ************* Administración Embarcaciones *************
                                    **
                                    --%>
                                    <c:when test="${abm=='embarcacion'}">
                                        <font color="gray" size="5">${abm} OK</font>
                                        <%--  
                                        <form method="post" action="ControladorABMServlets">
                                            <select name="abm" >
                                                <option value="ingresarEmbarcacion">Ingresar Embarcación</option>
                                                <option value="modificarEmbarcacion">Modificar Embarcación</option>
                                                <option value="datosEmbarcacion">Vista datos Embarcaciónes</option>
                                            </select>

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">
                                            <input type="submit"  value="Solicitud">
                                        </form>    
                                        --%>
                                    </c:when>
                                    <%-- 
                                    **
                                    ************* Administración Amarres *************
                                    **
                                    --%>
                                    <c:when test="${abm=='amarre'}">
                                        <font color="gray" size="5">${abm} OK</font>
                                        <%-- 
                                        <form method="post" action="ControladorABMServlets">
                                            <select name="abm" >
                                                <option value="ingresarAmarre">Ingresar Amarre</option>
                                                <option value="modificarAmarre">Modificar Amarre</option>
                                                <option value="datosAmarre">Vista datos Amarres</option>
                                            </select>

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">
                                            <input type="submit"  value="Solicitud">
                                        </form>  
                                        --%>
                                    </c:when>
                                    <%-- 
                                    **
                                    ************* Administración Zonas *************
                                    **
                                    --%>
                                    <c:when test="${abm=='zona' or abm=='nuevoRegistroZona' or abm=='modificarRegistroZona' or abm=='eliminarRegistroZona'}">
                                        <c:choose>
                                            <c:when test="${abm=='zona'}">
                                                <font color="gray" size="5">${abm}</font>
                                            </c:when>
                                            <c:otherwise>
                                                <font color="gray" size="5">${abm} se realizo correctamente!</font>
                                            </c:otherwise>
                                        </c:choose>
                                        <p></p>
                                        <%--  
                                        <form method="post" action="ControladorZonaServlets">
                                            <select name="abm" >
                                                <option value="ingresarZona">Ingresar Zona</option>
                                                <option value="modificarZona">Modificar Zona</option>
                                                <option value="eliminarZona">Eliminar Zona</option>
                                                <option value="datosZona">Vista datos Zonas</option>
                                            </select>

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">
                                            <input type="submit"  value="Solicitud">
                                        </form>    
                                        --%>
                                    </c:when>
                                    <c:when test="${abm=='ingresarZona'}">
                                        <form method="post" action="ControladorZonaServlets">
                                            <font color="gray" size="5">${abm}</font>
                                            <input type="text" name="letra" placeholder="Letra">
                                            <select name="tipo" >
                                                <option value="Velero clasico">Velero clasico</option>
                                                <option value="Velero moderno">Velero moderno</option>
                                                <option value="Catamaranes">Catamaranes</option>
                                                <option value="Yates a motor">Yates a motor</option>
                                                <option value="Yates de lujo">Yates de lujo</option>
                                                <option value="Mega yates">Mega yates</option>
                                            </select>
                                            <input type="text" name="cantidad" placeholder="Cantidad de barcos" >
                                            <input type="text" name="profundidad" placeholder="Profundidad amarre (metros)" >
                                            <input type="text" name="ancho" placeholder="Ancho del amarre (metros)" >
                                            <select name="estado" >
                                                <option value="Habilitado">Habilitado</option>
                                                <option value="Deshabilitado">Deshabilitado</option>
                                            </select>

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">

                                            <input type="hidden" name="abm" value="nuevoRegistroZona">
                                            <input type="submit"  value="Solicitud">
                                        </form>    
                                    </c:when>
                                    <c:when test="${abm=='modificarZona'}">
                                        <c:choose>
                                            <c:when test="${empty resultadosABM}">
                                                <h1>ResultadosABM vacio</h1>
                                            </c:when>
                                            <c:otherwise>
                                                <font color="gray" size="5">${abm}</font>
                                                <table class="egt">
                                                    <tr>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal">Id</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray" >Letra</font> </td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Tipo de barco</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Cantidad de barcos</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Profundidad amarres (metros)</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Ancho amarres (metros)</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Estado</font> </td>
                                                    </tr>
                                                    <c:forEach var="resultados" items="${resultadosABM}">
                                                        <tr>
                                                            <td><font color="teal">${resultados.id} </font></td>
                                                            <td><font color="gray">${resultados.letra} </font> </td>
                                                            <td><font color="gray">${resultados.tipo} </font></td>
                                                            <td><font color="gray">${resultados.cantidad} </font></td>
                                                            <td><font color="gray">${resultados.profundidad} </font></td>
                                                            <td><font color="gray">${resultados.ancho} </font></td>
                                                                <c:choose>
                                                                    <c:when test="${resultados.estado=='Habilitado'}">
                                                                    <td><font color="gray">${resultados.estado} </font> </td>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                    <td><font color="red">${resultados.estado} </font> </td>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                        </tr>
                                                    </c:forEach>
                                                </table>
                                            </c:otherwise>
                                        </c:choose>  
                                        <form method="post" action="ControladorZonaServlets">
                                            <font color="gray" size="5">${abm}</font>
                                            <input type="text" name="id" placeholder="ID">
                                            <input type="text" name="letra" placeholder="Letra">
                                            <select name="tipo" >
                                                <option value="Velero clasico">Velero clasico</option>
                                                <option value="Velero moderno">Velero moderno</option>
                                                <option value="Catamaranes">Catamaranes</option>
                                                <option value="Yates a motor">Yates a motor</option>
                                                <option value="Yates de lujo">Yates de lujo</option>
                                                <option value="Mega yates">Mega yates</option>
                                            </select>
                                            <input type="text" name="cantidad" placeholder="Cantidad de barcos" >
                                            <input type="text" name="profundidad" placeholder="Profundidad amarre (metros)" >
                                            <input type="text" name="ancho" placeholder="Ancho del amarre (metros)" >
                                            <select name="estado" >
                                                <option value="Habilitado">Habilitado</option>
                                                <option value="Deshabilitado">Deshabilitado</option>
                                            </select>

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">

                                            <input type="hidden" name="abm" value="modificarRegistroZona">
                                            <input type="submit"  value="Solicitud">  
                                        </form>
                                    </c:when>
                                    <c:when test="${abm=='eliminarZona'}">
                                        <c:choose>
                                            <c:when test="${empty resultadosABM}">
                                                <h1>ResultadosABM vacio</h1>
                                            </c:when>
                                            <c:otherwise>
                                                <font color="gray" size="5">${abm}</font>
                                                <table class="egt">
                                                    <tr>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal">Id</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray" >Letra</font> </td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Tipo de barco</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Cantidad de barcos</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Profundidad amarres (metros)</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Ancho amarres (metros)</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Estado</font> </td>
                                                    </tr>
                                                    <c:forEach var="resultados" items="${resultadosABM}">
                                                        <tr>
                                                            <td><font color="teal">${resultados.id} </font></td>
                                                            <td><font color="gray">${resultados.letra} </font> </td>
                                                            <td><font color="gray">${resultados.tipo} </font></td>
                                                            <td><font color="gray">${resultados.cantidad} </font></td>
                                                            <td><font color="gray">${resultados.profundidad} </font></td>
                                                            <td><font color="gray">${resultados.ancho} </font></td>
                                                                <c:choose>
                                                                    <c:when test="${resultados.estado=='Habilitado'}">
                                                                    <td><font color="gray">${resultados.estado} </font> </td>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                    <td><font color="red">${resultados.estado} </font> </td>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                        </tr>
                                                    </c:forEach>
                                                </table>
                                            </c:otherwise>
                                        </c:choose>  
                                        <form method="post" action="ControladorZonaServlets">
                                            <font color="gray">Seleccionar registro para eliminar </font>
                                            <input type="text" name="id" placeholder="ID">

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">

                                            <input type="hidden" name="abm" value="eliminarRegistroZona">
                                            <input type="submit"  value="Solicitud">
                                        </form>   
                                    </c:when>
                                    <c:when test="${abm=='datosZona'}">
                                        <c:choose>
                                            <c:when test="${empty resultadosABM}">
                                                <h1>ResultadosABM vacio</h1>
                                            </c:when>
                                            <c:otherwise>
                                                <font color="gray" size="5">${abm}</font>
                                                <table class="egt">
                                                    <tr>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal">Id</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray" >Letra</font> </td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Tipo de barco</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Cantidad de barcos</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Profundidad amarres (metros)</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Ancho amarres (metros)</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Estado</font> </td>
                                                    </tr>
                                                    <c:forEach var="resultados" items="${resultadosABM}">
                                                        <tr>
                                                            <td><font color="teal">${resultados.id} </font></td>
                                                            <td><font color="gray">${resultados.letra} </font> </td>
                                                            <td><font color="gray">${resultados.tipo} </font></td>
                                                            <td><font color="gray">${resultados.cantidad} </font></td>
                                                            <td><font color="gray">${resultados.profundidad} </font></td>
                                                            <td><font color="gray">${resultados.ancho} </font></td>
                                                                <c:choose>
                                                                    <c:when test="${resultados.estado=='Habilitado'}">
                                                                    <td><font color="gray">${resultados.estado} </font> </td>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                    <td><font color="red">${resultados.estado} </font> </td>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                        </tr>
                                                    </c:forEach>
                                                </table>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <%-- 
                                    **
                                    ************* Administración Administradores *************
                                    **
                                    --%>
                                    <c:when test="${abm=='administrador' or abm=='modificarRegistro' or abm=='nuevoRegistro' or abm=='eliminarRegistro'}">      
                                        <c:choose>
                                            <c:when test="${abm=='administrador'}">
                                                <font color="gray" size="5">${abm}</font>
                                            </c:when>
                                            <c:otherwise>
                                                <font color="gray" size="5">${abm} se realizo correctamente!</font>
                                            </c:otherwise>
                                        </c:choose>
                                        <%--  
                                <form method="post" action="ControladorABMServlets">
                                    <select name="abm" >
                                        <option value="ingresarAdministrador">Ingresar Administrador</option>
                                        <option value="modificarAdministrador">Modificar Administrador</option>
                                        <option value="eliminarAdministrador">Eliminar Administrador</option>
                                        <option value="datosAdministrador">Vista datos Administrador</option>
                                    </select>

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">
                                            <input type="submit"  value="Solicitud">
                                        </form>    
                                        --%>
                                    </c:when>
                                    <c:when test="${abm=='ingresarAdministrador'}">
                                        <form method="post" action="ControladorABMServlets">
                                            <font color="gray" size="5">${abm}</font>
                                            <input type="text" name="nombre" placeholder="Nombre">
                                            <input type="text" name="dni" placeholder="DNI">
                                            <input type="text" name="direccion" placeholder="Direccion" >
                                            <input type="text" name="ingreso" placeholder=" dd/mm/aaaa" >
                                            <input type="text" name="password" placeholder="Contraseña" >
                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">

                                            <input type="hidden" name="abm" value="nuevoRegistro">
                                            <input type="submit"  value="Solicitud">
                                        </form>    
                                    </c:when>
                                    <c:when test="${abm=='modificarAdministrador'}">
                                        <c:choose>
                                            <c:when test="${empty resultadosABM}">
                                                <h1>ResultadosABM vacio</h1>
                                            </c:when>
                                            <c:otherwise>
                                                <font color="gray" size="5">datosAdministrador</font>
                                                <table class="egt">
                                                    <tr>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal">Id</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray" >Nombre</font> </td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">DNI</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Dirección</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Ingreso</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Contraseña</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Estado</font> </td>
                                                    </tr>
                                                    <c:forEach var="resultados" items="${resultadosABM}">
                                                        <tr>
                                                            <td><font color="teal">${resultados.id} </font></td>
                                                            <td><font color="gray">${resultados.nombre} </font> </td>
                                                            <td><font color="gray">${resultados.dni} </font></td>
                                                            <td><font color="gray">${resultados.direccion} </font></td>
                                                            <td><font color="gray">${resultados.ingreso} </font></td>
                                                            <td><font color="gray">${resultados.password} </font></td>
                                                                <c:choose>
                                                                    <c:when test="${resultados.estado=='Habilitado'}">
                                                                    <td><font color="gray">${resultados.estado} </font> </td>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                    <td><font color="red">${resultados.estado} </font> </td>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                        </tr>
                                                    </c:forEach>
                                                </table>
                                            </c:otherwise>
                                        </c:choose>  
                                        <font color="gray" size="5">${abm}</font> 
                                        <form method="post" action="ControladorABMServlets">

                                            <input type="text" name="id" placeholder="ID">
                                            <input type="text" name="nombre" placeholder="Nombre">
                                            <input type="text" name="dni" placeholder="DNI">
                                            <input type="text" name="direccion" placeholder="Direccion" >
                                            <input type="text" name="ingreso" placeholder=" dd/mm/aaaa" >
                                            <input type="text" name="password" placeholder="Contraseña" >
                                            <select name="estado" >
                                                <option value="Habilitado">Habilitado</option>
                                                <option value="Deshabilitado">Deshabilitado</option>
                                            </select>

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">

                                            <input type="hidden" name="abm" value="modificarRegistro">
                                            <input type="submit"  value="Solicitud">
                                        </form>   
                                    </c:when>
                                    <c:when test="${abm=='eliminarAdministrador'}">
                                        <c:choose>
                                            <c:when test="${empty resultadosABM}">
                                                <h1>ResultadosABM vacio</h1>
                                            </c:when>
                                            <c:otherwise>
                                                <font color="gray" size="5">datosAdministrador</font>
                                                <table class="egt">
                                                    <tr>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal">Id</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray" >Nombre</font> </td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">DNI</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Dirección</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Ingreso</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Contraseña</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Estado</font> </td>
                                                    </tr>
                                                    <c:forEach var="resultados" items="${resultadosABM}">
                                                        <tr>
                                                            <td><font color="teal">${resultados.id} </font></td>
                                                            <td><font color="gray">${resultados.nombre} </font> </td>
                                                            <td><font color="gray">${resultados.dni} </font></td>
                                                            <td><font color="gray">${resultados.direccion} </font></td>
                                                            <td><font color="gray">${resultados.ingreso} </font></td>
                                                            <td><font color="gray">${resultados.password} </font></td>
                                                                <c:choose>
                                                                    <c:when test="${resultados.estado=='Habilitado'}">
                                                                    <td><font color="gray">${resultados.estado} </font> </td>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                    <td><font color="red">${resultados.estado} </font> </td>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                        </tr>
                                                    </c:forEach>
                                                </table>
                                            </c:otherwise>
                                        </c:choose>  
                                        <font color="gray" size="5">${abm}</font>
                                        <form method="post" action="ControladorABMServlets">
                                            <font color="gray">Seleccionar registro para eliminar </font>
                                            <input type="text" name="id" placeholder="ID">

                                            <input type="hidden" name="dirIP" value="localhost">
                                            <input type="hidden" name="nomBD" value="databaseclub">

                                            <input type="hidden" name="abm" value="eliminarRegistro">
                                            <input type="submit"  value="Solicitud">
                                        </form>   
                                    </c:when>
                                    <c:when test="${abm=='datosAdministrador'}">
                                        <c:choose>
                                            <c:when test="${empty resultadosABM}">
                                                <h1>ResultadosABM vacio</h1>
                                            </c:when>
                                            <c:otherwise>
                                                <font color="gray" size="5">${abm}</font>
                                                <table class="egt">
                                                    <tr>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal">Id</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray" >Nombre</font> </td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">DNI</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Dirección</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Ingreso</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Contraseña</font></td>
                                                        <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Estado</font> </td>
                                                    </tr>
                                                    <c:forEach var="resultados" items="${resultadosABM}">
                                                        <tr>
                                                            <td><font color="teal">${resultados.id} </font></td>
                                                            <td><font color="gray">${resultados.nombre} </font> </td>
                                                            <td><font color="gray">${resultados.dni} </font></td>
                                                            <td><font color="gray">${resultados.direccion} </font></td>
                                                            <td><font color="gray">${resultados.ingreso} </font></td>
                                                            <td><font color="gray">${resultados.password} </font></td>
                                                                <c:choose>
                                                                    <c:when test="${resultados.estado=='Habilitado'}">
                                                                    <td><font color="gray">${resultados.estado} </font> </td>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                    <td><font color="red">${resultados.estado} </font> </td>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                        </tr>
                                                    </c:forEach>
                                                </table>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>
                    </section>

                </div>
            </div>
            <!-- Sidebar -->
            <div id="sidebar">
                <div class="inner">


                    <!-- Menu -->
                    <nav id="menu">
                        <header class="major">
                            <h2><a href="http://localhost:8080/ClubVictoriaWeb/">Salir</a></h2>
                        </header>
                        <ul>
                            <form id="myformSocio" method="post" action="ControladorSocioServlets">
                                <input type="hidden" name="dirIP" value="localhost">
                                <input type="hidden" name="nomBD" value="databaseclub">
                                <input id="abmSocio" type="hidden" name="abm" value="">
                            </form>

                            <li>
                                <span class="opener">Socios</span>
                                <ul>
                                    <li><a onclick="document.getElementById('abmSocio').value = 'ingresarSocio';document.getElementById('myformSocio').submit();">Ingresar Socio</a></li>
                                    <li><a onclick="document.getElementById('abmSocio').value = 'modificarSocio';document.getElementById('myformSocio').submit();">Modificar Socio</a></li>
                                    <li><a onclick="document.getElementById('abmSocio').value = 'eliminarSocio';document.getElementById('myformSocio').submit();">Eliminar Socio</a></li>
                                    <li><a onclick="document.getElementById('abmSocio').value = 'datosSocio';document.getElementById('myformSocio').submit();">Vista datos Socios</a></li>

                                </ul>
                            </li>
                        </ul>
                        <ul> 
                            <form id="myformEmpleado" method="post" action="ControladorEmpleadoServlets">
                                <input type="hidden" name="dirIP" value="localhost">
                                <input type="hidden" name="nomBD" value="databaseclub">
                                <input id="abmEmpleado" type="hidden" name="abm" value="">
                            </form>
                            <li>
                                <span class="opener">Empleados</span>
                                <ul>
                                    <li><a onclick="document.getElementById('abmEmpleado').value = 'ingresarEmpleado';document.getElementById('myformEmpleado').submit();">Ingresar Empleado</a></li>
                                    <li><a onclick="document.getElementById('abmEmpleado').value = 'modificarEmpleado';document.getElementById('myformEmpleado').submit();">Modificar Empleado</a></li>
                                    <li><a onclick="document.getElementById('abmEmpleado').value = 'eliminarEmpleado';document.getElementById('myformEmpleado').submit();">Eliminar Empleado</a></li>
                                    <li><a onclick="document.getElementById('abmEmpleado').value = 'datosEmpleado';document.getElementById('myformEmpleado').submit();">Vista datos Empleados</a></li>

                                </ul>
                            </li>
                        </ul>
                        <ul> 
                            <form id="myformEmbarcacion" method="post" action="ControladorEmbarcacionServlets">
                                <input type="hidden" name="dirIP" value="localhost">
                                <input type="hidden" name="nomBD" value="databaseclub">
                                <input id="abmEmbarcacion" type="hidden" name="abm" value="">
                            </form>
                            <li>
                                <span class="opener">Embarcaciones</span>
                                <ul>
                                    <li><a onclick="document.getElementById('abmEmbarcacion').value = 'ingresarEmbarcacion';document.getElementById('myformEmbarcacion').submit();">Ingresar Embarcación</a></li>
                                    <li><a onclick="document.getElementById('abmEmbarcacion').value = 'modificarEmbarcacion';document.getElementById('myformEmbarcacion').submit();">Modificar Embarcación</a></li>
                                    <li><a onclick="document.getElementById('abmEmbarcacion').value = 'eliminarEmbarcacion';document.getElementById('myformEmbarcacion').submit();">Eliminar Embarcación</a></li>
                                    <li><a onclick="document.getElementById('abmEmbarcacion').value = 'datosEmbarcacion';document.getElementById('myformEmbarcacion').submit();">Vista datos Embarcaciones</a></li>

                                </ul>
                            </li></ul>
                        <ul> 
                            <form id="myformAmarre" method="post" action="ControladorAmarreServlets">
                                <input type="hidden" name="dirIP" value="localhost">
                                <input type="hidden" name="nomBD" value="databaseclub">
                                <input id="abmAmarre" type="hidden" name="abm" value="">
                            </form>
                            <li>
                                <span class="opener">Amarres</span>
                                <ul>
                                    <li><a onclick="document.getElementById('abmAmarre').value = 'ingresarAmarre';document.getElementById('myformAmarre').submit();">Ingresar Amarre</a></li>
                                    <li><a onclick="document.getElementById('abmAmarre').value = 'modificarAmarre';document.getElementById('myformAmarre').submit();">Modificar Amarre</a></li>
                                    <li><a onclick="document.getElementById('abmAmarre').value = 'eliminarAmarre';document.getElementById('myformAmarre').submit();">Eliminar Amarre</a></li>
                                    <li><a onclick="document.getElementById('abmAmarre').value = 'datosAmarre';document.getElementById('myformAmarre').submit();">Vista datos Amarres</a></li>

                                </ul>
                            </li>
                        </ul>
                        <ul> 
                            <form id="myformZona" method="post" action="ControladorZonaServlets">
                                <input type="hidden" name="dirIP" value="localhost">
                                <input type="hidden" name="nomBD" value="databaseclub">
                                <input id="abmZona" type="hidden" name="abm" value="">
                            </form>
                            <li>
                                <span class="opener">Zonas</span>
                                <ul>
                                    <li><a onclick="document.getElementById('abmZona').value = 'ingresarZona';document.getElementById('myformZona').submit();">Ingresar Zona</a></li>
                                    <li><a onclick="document.getElementById('abmZona').value = 'modificarZona';document.getElementById('myformZona').submit();">Modificar Zona</a></li>
                                    <li><a onclick="document.getElementById('abmZona').value = 'eliminarZona';document.getElementById('myformZona').submit();">Eliminar Zona</a></li>
                                    <li><a onclick="document.getElementById('abmZona').value = 'datosZona';document.getElementById('myformZona').submit();">Vista datos Zonas</a></li>

                                </ul>
                            </li>
                        </ul>
                        <ul> 
                            <form id="myformAdministrador" method="post" action="ControladorABMServlets">
                                <input type="hidden" name="dirIP" value="localhost">
                                <input type="hidden" name="nomBD" value="databaseclub">
                                <input id="abmAdministrador" type="hidden" name="abm" value="">
                            </form>
                            <li>
                                <span class="opener">Administrador</span>
                                <ul>

                                    <li><a onclick="document.getElementById('abmAdministrador').value = 'ingresarAdministrador';document.getElementById('myformAdministrador').submit();">Ingresar Administrador</a></li>
                                    <li><a onclick="document.getElementById('abmAdministrador').value = 'modificarAdministrador';document.getElementById('myformAdministrador').submit();">Modificar Administrador</a></li>
                                    <li><a onclick="document.getElementById('abmAdministrador').value = 'eliminarAdministrador';document.getElementById('myformAdministrador').submit();">Eliminar Administrador</a></li>
                                    <li><a onclick="document.getElementById('abmAdministrador').value = 'datosAdministrador';document.getElementById('myformAdministrador').submit();">Vista datos Administrador</a></li>

                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

        </div>
        <!-- Scripts -->
        <script src="editorial/assets/js/jquery.min.js"></script>
        <script src="editorial/assets/js/browser.min.js"></script>
        <script src="editorial/assets/js/breakpoints.min.js"></script>
        <script src="editorial/assets/js/util.js"></script>
        <script src="editorial/assets/js/main.js"></script>
    </body>
</html>