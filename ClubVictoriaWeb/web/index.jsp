
<!DOCTYPE HTML>
<!--
        Story by HTML5 UP
        html5up.net | @ajlkn
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <title>Club Nautico - Login</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="story/assets/css/main.css" />
        <noscript><link rel="stylesheet" href="story/assets/css/noscript.css" /></noscript>
    </head>
    <body class="is-preload">



        <section class="banner style2 orient-center content-align-center image-position-right fullscreen onload-image-fade-in onload-content-fade-down">
            <div class="content">
                <h1>Club Nautico</h1>
                <p class="major"></p>
                <form method="post" action="ControladorLoginServlets">
                    <input type="hidden" name="dirIP" value="localhost">
                    <input type="hidden" name="nomBD" value="databaseclub">
                    <div class="fields">
                        <div class="field align-left">
                            <label for="name" >Usuario</label>
                            <input type="text" name="nombre" id="nombre" value="" width="12" autofocus="" placeholder="Usuario" required />
                        </div>
                        <div class="field align-left">
                            <label for="name">Password</label>
                            <input type="password" name="password" id="pass" value="" width="12" autofocus="" placeholder="Contraseņa" required />
                        </div>
                        <div class="field">
                            <input type="submit"  value="Login">
                        </div>
                    </div>
                </form>
            </div>
            <div class="image">
                <img src="story/images/banner.jpg" alt="" />
            </div>
        </section>


        <!-- Scripts -->
        <script src="story/assets/js/jquery.min.js"></script>
        <script src="story/assets/js/jquery.scrollex.min.js"></script>
        <script src="story/assets/js/jquery.scrolly.min.js"></script>
        <script src="story/assets/js/browser.min.js"></script>
        <script src="story/assets/js/breakpoints.min.js"></script>
        <script src="story/assets/js/util.js"></script>
        <script src="story/assets/js/main.js"></script>

    </body>
</html>