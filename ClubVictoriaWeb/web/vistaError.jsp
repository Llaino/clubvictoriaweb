<%-- 
    Document   : vistaError
    Created on : Nov 3, 2019, 7:52:41 PM
    Author     : vivi
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ERROR JSP Page</title>
        <style type="text/css">
            .boton-personalizazo-5{
                text-decoration: none;
                font-size: 20px;
                color:#000000;
                background-color: transparent;
                border-width: 1px;
                border-style: solid;
                border-color: #000000;
            }
            
        </style> 
        
    </head>
    <body>
        <%--
        boton Salir
        --%>
        <a class="boton-personalizazo-5" href="http://localhost:8080/ClubVictoriaWeb/">Salir</a>
        <p></p>
        <p></p>
        <c:choose>
            <c:when test="${empty error}">
                <font color="gray" size="5" >Error vacio</font>
            </c:when>
            <c:otherwise>
                <font color="gray" size="5" >${error}</font>
            </c:otherwise>
        </c:choose>
      
    </body>
</html>
