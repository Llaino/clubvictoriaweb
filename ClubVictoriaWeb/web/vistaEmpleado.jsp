<%-- 
    Document   : vista
    Created on : Oct 13, 2019, 7:49:56 PM
    Author     : vivi
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>



<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>vistaEmpleadoJSP</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no,text/html; charset=UTF-8" />
        <link rel="stylesheet" href="editorial/assets/css/main.css" />
    </head>

    <body>
        <%-- 
        ----------------- Boton Salir -----------------
        <a class="boton-personalizado-5" href="http://localhost:8080/ClubVictoriaWeb/">Salir</a> 
        --%>

        <%-- 
        ----------------- Login ----------------- 
        --%>
        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->
            <div id="main">
                <div class="inner">
                    <c:choose>
                        <c:when test="${empty nombre}">
                            <header id="header">
                                <a class="logo"><strong>nombre vacio</strong></a>
                            </header>
                        </c:when> 
                        <c:otherwise>
                            <header id="header">
                                <a class="logo">${nombre}</a>
                            </header>
                        </c:otherwise>
                    </c:choose>


                    <!-- Content -->
                    <c:choose>
                        <c:when test="${empty nombre}">
                        </c:when> 
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${empty resultadosLogin}">
                                    <h1>Error. No hay resultados Login. Verificar conexión a base de datos.</h1>
                                </c:when>
                                <c:otherwise>
                                    <font color="gray" size="5">Mis Datos</font>
                                    <table class="egt">
                                        <tr>
                                            <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal">Id</font></td>
                                            <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray" >idUsuario</font> </td>
                                            <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray" >Nombre</font> </td>
                                            <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Tipo</font></td>
                                            <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Contraseña</font></td>
                                            <td style="background: rgba(255, 128, 0, 0.3); border: 1px solid rgba(200, 100, 0, 0.3);"><font color="teal"><font color="gray">Estado</font> </td>
                                        </tr>
                                        <c:forEach var="resultados" items="${resultadosLogin}">
                                            <tr>
                                                <td><font color="teal">${resultados.id} </font></td>
                                                <td><font color="teal">${resultados.idUsuario} </font></td>
                                                <td><font color="gray">${resultados.nombre} </font> </td>
                                                <td><font color="gray">${resultados.tipo} </font></td>
                                                <td><font color="gray">${resultados.password} </font></td>
                                                    <c:choose>
                                                        <c:when test="${resultados.estado=='Habilitado'}">
                                                        <td><font color="gray">${resultados.estado} </font> </td>
                                                        </c:when>
                                                        <c:otherwise>
                                                        <td><font color="red">${resultados.estado} </font> </td>
                                                        </c:otherwise>
                                                    </c:choose>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <!-- Sidebar -->
            <div id="sidebar">
                <div class="inner">


                    <!-- Menu -->
                    <nav id="menu">
                        <header class="major">
                            <h2><a href="http://localhost:8080/ClubVictoriaWeb/">Salir</a></h2>
                        </header>
                        <ul> 
                            <form id="myformEmpleado" method="post" action="ControladorEmpleadoServlets">
                                <input type="hidden" name="dirIP" value="localhost">
                                <input type="hidden" name="nomBD" value="databaseclub">
                                <input id="abmEmpleado" type="hidden" name="abm" value="">
                            </form>
                            <li>
                                <span class="opener">Empleados</span>
                                <ul>
                                    <li><a onclick="document.getElementById('abmEmpleado').value = '';document.getElementById('myformEmpleado').submit();">Mis horarios</a></li>
                                    <li><a onclick="document.getElementById('abmEmpleado').value = '';document.getElementById('myformEmpleado').submit();">Mis tareas</a></li>
                                    <li><a onclick="document.getElementById('abmEmpleado').value = '';document.getElementById('myformEmpleado').submit();">Mis Zonas</a></li>
                                    <li><a onclick="document.getElementById('abmEmpleado').value = '';document.getElementById('myformEmpleado').submit();">Modificaciones</a></li>

                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

        </div>
        <!-- Scripts -->
        <script src="editorial/assets/js/jquery.min.js"></script>
        <script src="editorial/assets/js/browser.min.js"></script>
        <script src="editorial/assets/js/breakpoints.min.js"></script>
        <script src="editorial/assets/js/util.js"></script>
        <script src="editorial/assets/js/main.js"></script>
    </body>
</html>