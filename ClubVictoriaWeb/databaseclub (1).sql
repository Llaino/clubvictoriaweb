-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-11-2019 a las 19:59:07
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `databaseclub`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administradores`
--

CREATE TABLE `administradores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `dni` varchar(30) DEFAULT NULL,
  `direccion` varchar(30) DEFAULT NULL,
  `ingreso` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `estado` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administradores`
--

INSERT INTO `administradores` (`id`, `nombre`, `dni`, `direccion`, `ingreso`, `password`, `estado`) VALUES
(27, 'olga45', '', '', '', 'olga45', 'Habilitado'),
(28, 'olga', '', '', '', 'olga', 'Habilitado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `amarres`
--

CREATE TABLE `amarres` (
  `id` int(11) NOT NULL,
  `numero` varchar(30) DEFAULT NULL,
  `agua` varchar(30) DEFAULT NULL,
  `luz` varchar(30) DEFAULT NULL,
  `servicios` varchar(30) DEFAULT NULL,
  `estado` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `embarcaciones`
--

CREATE TABLE `embarcaciones` (
  `id` int(11) NOT NULL,
  `matricula` varchar(30) DEFAULT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `tipo` varchar(30) DEFAULT NULL,
  `dimensiones` varchar(30) DEFAULT NULL,
  `estado` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id` int(11) NOT NULL,
  `codigo` varchar(30) DEFAULT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `direccion` varchar(30) DEFAULT NULL,
  `telefono` varchar(30) DEFAULT NULL,
  `especialidad` varchar(30) DEFAULT NULL,
  `password` varchar(30) NOT NULL,
  `estado` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id`, `codigo`, `nombre`, `direccion`, `telefono`, `especialidad`, `password`, `estado`) VALUES
(1, '12', 'empleado1', '', '', '', 'empleado1', 'Habilitado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id` int(11) DEFAULT NULL,
  `caracteristica` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso_embarcacion`
--

CREATE TABLE `permiso_embarcacion` (
  `id` int(11) NOT NULL,
  `idZona` int(10) DEFAULT NULL,
  `idAmarre` int(10) DEFAULT NULL,
  `fechaEmbarcacionAmarre` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso_empleado`
--

CREATE TABLE `permiso_empleado` (
  `id` int(11) NOT NULL,
  `idEmpleado` int(10) DEFAULT NULL,
  `idZona` int(10) DEFAULT NULL,
  `idEmbarcacion` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso_socio`
--

CREATE TABLE `permiso_socio` (
  `id` int(11) NOT NULL,
  `idSocio` int(10) DEFAULT NULL,
  `idEmbarcacion` int(10) DEFAULT NULL,
  `idAmarre` int(10) DEFAULT NULL,
  `fechaSocioEmbarcacion` varchar(30) DEFAULT NULL,
  `fechaSocioAmarre` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `socios`
--

CREATE TABLE `socios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `dni` varchar(30) DEFAULT NULL,
  `direccion` varchar(30) DEFAULT NULL,
  `telefono` varchar(30) DEFAULT NULL,
  `ingreso` varchar(30) DEFAULT NULL,
  `password` varchar(30) NOT NULL,
  `estado` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `socios`
--

INSERT INTO `socios` (`id`, `nombre`, `dni`, `direccion`, `telefono`, `ingreso`, `password`, `estado`) VALUES
(5, 'socio1', '', '', NULL, '', 'socio1', 'Habilitado'),
(6, 'socio2', '', '', '', '', 'socio2', 'Deshabilitado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `idUsuario` varchar(30) DEFAULT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `tipo` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `estado` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `idUsuario`, `nombre`, `tipo`, `password`, `estado`) VALUES
(5, '5', 'olga5', 'Administrador', 'pass5', 'Habilitado'),
(6, '1', 'nombre', 'Administrador', ' password ', 'Habilitado'),
(7, '1', 'usuario6', 'Administrador', 'usuario6', 'Habilitado'),
(8, '1', 'usuario7', 'Administrador', 'usuario7', 'Habilitado'),
(11, '5', 'olga29', 'Administrador', 'pass29', 'Habilitado'),
(12, '26', 'olgavivi', 'Administrador', 'olgavivi', 'Habilitado'),
(13, '27', 'olga45', 'Administrador', 'olga45', 'Habilitado'),
(14, '4', 'socio1', 'Socio', 'socio1', 'Habilitado'),
(16, '28', 'olga', 'Administrador', 'olga', 'Habilitado'),
(17, '6', 'socio2', 'Socio', 'socio2', 'Deshabilitado'),
(18, '1', 'empleado1', 'Empleado', 'empleado1', 'Habilitado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zonas`
--

CREATE TABLE `zonas` (
  `id` int(11) NOT NULL,
  `letra` varchar(30) DEFAULT NULL,
  `tipo` varchar(30) DEFAULT NULL,
  `cantidad` varchar(30) DEFAULT NULL,
  `profundidad` varchar(30) DEFAULT NULL,
  `ancho` varchar(30) DEFAULT NULL,
  `estado` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `zonas`
--

INSERT INTO `zonas` (`id`, `letra`, `tipo`, `cantidad`, `profundidad`, `ancho`, `estado`) VALUES
(1, 'A', 'Yates de lujo', '2', '100', '100', 'Habilitado'),
(3, 'B', 'Mega yates', '1', '200', '200', 'Habilitado'),
(4, 'D', 'Catamaranes', '5', '100', '100', 'Deshabilitado'),
(5, 'V', 'Mega yates', '2', '100', '100', 'Deshabilitado'),
(6, 'S', 'Velero clasico', '10', '100', '100', 'Deshabilitado');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administradores`
--
ALTER TABLE `administradores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `amarres`
--
ALTER TABLE `amarres`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `embarcaciones`
--
ALTER TABLE `embarcaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permiso_embarcacion`
--
ALTER TABLE `permiso_embarcacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permiso_empleado`
--
ALTER TABLE `permiso_empleado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permiso_socio`
--
ALTER TABLE `permiso_socio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `socios`
--
ALTER TABLE `socios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `zonas`
--
ALTER TABLE `zonas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administradores`
--
ALTER TABLE `administradores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `amarres`
--
ALTER TABLE `amarres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `embarcaciones`
--
ALTER TABLE `embarcaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `permiso_embarcacion`
--
ALTER TABLE `permiso_embarcacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `permiso_empleado`
--
ALTER TABLE `permiso_empleado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `permiso_socio`
--
ALTER TABLE `permiso_socio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `socios`
--
ALTER TABLE `socios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `zonas`
--
ALTER TABLE `zonas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
